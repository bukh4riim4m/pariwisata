<?php
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
Route::get('/tes',['as'=>'tes','uses'=>'DepanController@tes']);

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });\
Route::get('/',['as'=>'index','uses'=>'DepanController@index']);

Auth::routes();
Route::get('/paket-tour/detail/{id}', ['as'=>'detail-tour','uses'=>'DepanController@detail']);
Route::post('/paket-tour/detail/{id}', ['as'=>'detail-tour','uses'=>'DepanController@detail']);
Route::get('/contact', ['as'=>'contact','uses'=>'DepanController@contact']);
Route::post('/contact', ['as'=>'contact','uses'=>'DepanController@contact']);
Route::get('/gallery', ['as'=>'gallery','uses'=>'DepanController@gallery']);
Route::get('/gallery/{id}', ['as'=>'gallery-detail','uses'=>'DepanController@gallerydetail']);
Route::get('/blog', ['as'=>'blog','uses'=>'DepanController@blog']);
Route::get('/blog/{id}', ['as'=>'blogdetail','uses'=>'DepanController@blogdetail']);
Route::get('/about', ['as'=>'about','uses'=>'DepanController@about']);
Route::get('/tours', ['as'=>'tours','uses'=>'DepanController@tours']);
Route::get('/faq', ['as'=>'faq','uses'=>'DepanController@faq']);
Route::get('/how_do_i_make_a_reservation', ['as'=>'reservation','uses'=>'DepanController@reservation']);
Route::get('/payment-option', ['as'=>'payment-option','uses'=>'DepanController@payment']);
Route::get('/booking-tips', ['as'=>'booking-tips','uses'=>'DepanController@booking']);
Route::post('/komentar/{id}', ['as'=>'komentar','uses'=>'DepanController@komentar']);
Route::post('/form-pesenger', ['as'=>'form-pesenger','uses'=>'DepanController@pesenger']);
Route::post('/informasi-transfer', ['as'=>'informasi-transfer','uses'=>'PemesananController@pemesan']);
Route::post('/download-invoice', ['as'=>'download-invoice','uses'=>'PemesananController@download']);
//ROUTE ADMIN
Route::get('/home', ['as'=>'home','uses'=>'Admin\AdminController@index']);
Route::get('/admin/home', ['as'=>'admin-home','uses'=>'Admin\AdminController@index']);
Route::get('/admin/sliders',['as'=>'admin-slider','uses'=>'Admin\SliderController@index']);
Route::get('/admin/slider/create',['as'=>'admin-slider-create','uses'=>'Admin\SliderController@create']);
Route::get('/admin/settings',['as'=>'admin-settings','uses'=>'Admin\SettingsController@index']);
Route::post('/admin/settings/slider',['as'=>'admin-settings-slider','uses'=>'Admin\SettingsController@index']);
Route::post('/admin/settings/logo',['as'=>'admin-settings-logo','uses'=>'Admin\SettingsController@logo']);
Route::post('/admin/settings/blog',['as'=>'admin-settings-blog','uses'=>'Admin\SettingsController@blog']);
Route::get('/admin/settings/blog/edit/{id}',['as'=>'admin-settings-blog-edit','uses'=>'Admin\SettingsController@editblog']);
Route::post('/admin/settings/blog/edit/{id}',['as'=>'admin-settings-blog-edit','uses'=>'Admin\SettingsController@editblog']);
Route::get('/admin/gallery',['as'=>'admin-gallery','uses'=>'Admin\GalleryController@gallery']);
Route::post('/admin/gallery',['as'=>'admin-gallery','uses'=>'Admin\GalleryController@gallery']);
Route::post('/admin/settings/contact',['as'=>'admin-setting-contact','uses'=>'Admin\SettingsController@contact']);

Route::get('/admin/gallery/detail/{id}',['as'=>'admin-gallery-anak','uses'=>'Admin\GalleryController@anakgallery']);
Route::post('/admin/gallery/detail/{id}',['as'=>'admin-gallery-anak','uses'=>'Admin\GalleryController@anakgallery']);


Route::get('/admin/paket-tour', ['as'=>'admin-paket-tour','uses'=>'Admin\PaketTourController@index']);
Route::post('/admin/paket-tour', ['as'=>'admin-paket-tour','uses'=>'Admin\PaketTourController@index']);
Route::get('/admin/paket-tour/add', ['as'=>'admin-add-tour','uses'=>'Admin\PaketTourController@formadd']);
Route::post('/admin/paket-tour/add', ['as'=>'admin-add-tour','uses'=>'Admin\PaketTourController@formadd']);
Route::get('/admin/paket-tour/detail/{id}', ['as'=>'admin-detail-tour','uses'=>'Admin\PaketTourController@detail']);
Route::post('/admin/paket-tour/detail/{id}', ['as'=>'admin-detail-tour','uses'=>'Admin\PaketTourController@detail']);
Route::post('/admin/paket-tour/description/{id}', ['as'=>'admin-detail-tour-description','uses'=>'Admin\PaketTourController@description']);
Route::get('/logout',['as'=>'admin-logout','uses'=>'DepanController@logout']);

Route::get('/admin/blog',['as'=>'admin-blog','uses'=>'Admin\AdminController@pageblog']);
Route::get('/admin/blog/add',['as'=>'admin-add-blog','uses'=>'Admin\AdminController@addblog']);
Route::post('/admin/blog/add',['as'=>'admin-add-blog','uses'=>'Admin\AdminController@addblog']);
Route::get('/admin/blog/edit/{id}',['as'=>'admin-blog-edit','uses'=>'Admin\AdminController@editeblog']);
Route::post('/admin/blog/edit/{id}',['as'=>'admin-blog-edit','uses'=>'Admin\AdminController@editeblog']);

Route::post('/admin/pax/edit/{id}',['as'=>'admin-tour-edit-pax','uses'=>'Admin\PaketTourController@editepax']);
