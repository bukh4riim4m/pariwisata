<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/users',function(Request $request){
  return App\User::get();
});
Route::get('/slider',['as'=>'slider','uses'=>'Api\DepanController@slider']);
Route::post('login',['as'=>'login','uses'=>'Api\DepanController@login']);
Route::get('/blogs',['as'=>'blogs','uses'=>'Api\DepanController@blogs']);
Route::get('/wisata',['as'=>'wisatabuatan','uses'=>'Api\DepanController@wisata']);
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
