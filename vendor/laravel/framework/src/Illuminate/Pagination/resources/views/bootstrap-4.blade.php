@if ($paginator->hasPages())
<div class="bottom-nav">
<div class="pager">
    <!-- <ul class="pagination" role="navigation"> -->
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <span class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                <span class="page-link current" aria-hidden="true">First page</span>
            </span>
        @else
            <span class="page-item">
                <a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">First page</a>
            </span>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <span class="page-item disabled" aria-disabled="true">{{ $element }}</span>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <span class="active current" aria-current="page">{{ $page }}</span>
                    @else
                        <span><a class="page-link" href="{{ $url }}">{{ $page }}</a></span>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <span class="page-item">
                <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">Last page</a>
            </span>
        @else
            <span class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                <span class="page-link current" aria-hidden="true">Last page</span>
            </span>
        @endif
    <!-- </ul> -->
  </div>
  </div>
@endif
