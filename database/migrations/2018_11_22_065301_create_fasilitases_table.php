<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFasilitasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facilitis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('paket_id');
            $table->string('name',100);
            $table->string('created_by',100);
            $table->string('updated_by',100)->nullable();
            $table->string('deleted_by',100)->nullable();
            $table->integer('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facilitis');
    }
}
