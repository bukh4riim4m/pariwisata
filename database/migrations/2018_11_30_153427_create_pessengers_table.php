<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePessengersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemesanans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_pemesan',150);
            $table->string('email_pemesan',150);
            $table->string('telp',15);
            $table->integer('paket_id');
            $table->string('invoice',10);
            $table->integer('jumlah_pax');
            $table->integer('total_harga');
            $table->enum('status', ['Pending', 'Success','Cancel']);
            $table->text('alasan')->nullable();
            $table->string('proses_by',150)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pessengers');
    }
}
