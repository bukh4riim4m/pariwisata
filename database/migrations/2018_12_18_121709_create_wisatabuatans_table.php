<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWisatabuatansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wisatabuatans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul',100);
            $table->string('categoty',100);
            $table->string('status',50);
            $table->longText('description');
            $table->longText('akses');
            $table->integer('fasilitaswisatabuatan_id');
            $table->integer('gallery_id');
            $table->string('lokasi',250);
            $table->integer('rating');
            $table->integer('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wisatabuatans');
    }
}
