<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blok extends Model
{
  protected $fillable = [
    'id','title','gambar','description','page','created_by','updated_by','deleted_by','active','created_at','updated_at'
  ];
}
