<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GambarWisataBuatan extends Model
{
  protected $fillable = [
    'id','wisata_buatan_id','title','gambar','created_at','updated_at','created_by','updated_by','deleted_by'
  ];
}
