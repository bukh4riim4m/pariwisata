<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faciliti extends Model
{
  protected $fillable = [
    'id','paket_id','name','created_by','updated_by','deleted_by','active','created_at','updated_at'
  ];
}
