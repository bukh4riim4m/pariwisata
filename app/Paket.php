<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paket extends Model
{
  protected $fillable = [
    'id','gambar_id','day','nama_paket','min_pax','max_pax','harga','description','lokasi','active','created_by','updated_by','created_at','updated_at'
  ];
  public function gambar(){
    return $this->hasOne('App\Gambar');
  }
}
