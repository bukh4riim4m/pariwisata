<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pemesanan extends Model
{
  protected $fillable = [
    'id','paket_id','nama_pemesan','email_pemesan','kode_unik','telp','pessenger_key','invoice','jumlah_pax','total_harga','bank_id','status','alasan','proses_by','created_at','updated_at'
  ];

  public function paketId(){
    return $this->belongsTo('App\Paket','paket_id');
  }
  public function bankId(){
    return $this->belongsTo('App\Bank','bank_id');
  }
}
