<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wisatabuatan extends Model
{
  protected $fillable = [
    'id','judul','categoty','status','description','akses','fasilitaswisatabuatan_id','gallery_id','lokasi','rating','is_active','created_at','updated_at','created_by','updated_by','deleted_by'
  ];
}
