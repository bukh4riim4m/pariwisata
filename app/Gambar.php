<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gambar extends Model
{
  protected $fillable = [
    'id','paket_id','gambar','active','created_by','updated_by','deleted_by','created_at','updated_at'
  ];
}
