<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
  protected $fillable = [
    'id','gambar','title','description','created_at','updated_at'
  ];
}
