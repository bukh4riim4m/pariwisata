<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Web extends Model
{
  protected $fillable = [
    'id','title_web','logo','url_logo','url_server','hp','telp','email','alamat','maps','token_server','created_at','updated_at'
  ];
}
