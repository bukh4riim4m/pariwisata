<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
  protected $fillable = [
    'id','utama_id','title','img_gallery','status','created_by','updated_by','deleted_by','created_at','updated_at'
  ];
}
