<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Paket;
use App\Gambar;
use App\Faciliti;
use Image;
use DB;
use Log;

class PaketTourController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
  public function index(Request $request){
    if ($request->action =='editpaket') {
      $this->validate($request, [
        'nama_paket'=>'required',
        'harga'=>'required',
        'lokasi'=>'required',
      ]);
      $hasil = explode('"',$request->lokasi);
      if ($request->file('gambar')) {
        $this->validate($request, [
          'nama_paket'=>'required',
          'harga'=>'required',
          'gambar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        DB::beginTransaction();
        try {
          $gambars = Gambar::where('paket_id',$request->ids)->first();
          // return $gambars;
          $destination_foto =public_path('/template/images/uploads/'.$gambars->gambar);
          if (file_exists($destination_foto)) {
              unlink($destination_foto);
          }
          $image = $request->file('gambar');
          $imageName = $image->getClientOriginalName();
          $fileName = date('YmdHis')."_".$imageName;
          $directory = public_path('/template/images/uploads/');
          $imageUrl = $directory.$fileName;
          Image::make($image)->resize(1920, 1280)->save($imageUrl);
          $gambars->gambar = $fileName;
          $gambars->updated_by = $request->user()->name;
          $gambars->update();
          $pakets = Paket::find($request->ids)->update([
            'nama_paket'=>$request->nama_paket,
            'harga'=>$request->harga,
            'lokasi'=>$hasil[1],
            'updated_by'=>$request->user()->name
          ]);
        } catch (\Exception $e) {
          Log::info('Gagal edit paket tour:'.$e->getMessage());
          DB::rollback();
          return redirect()->route('admin-paket-tour');
        }
        DB::commit();
        return redirect()->route('admin-paket-tour');
      }

      DB::beginTransaction();
      try {
        $pakets = Paket::find($request->ids)->update([
          'nama_paket'=>$request->nama_paket,
          'harga'=>$request->harga,
          'lokasi'=>$hasil[1],
          'updated_by'=>$request->user()->name
        ]);
      } catch (\Exception $e) {
        Log::info('Gagal edit paket tour:'.$e->getMessage());
        DB::rollback();
        return redirect()->route('admin-paket-tour');
      }
      DB::commit();
      return redirect()->route('admin-paket-tour');
    }
    $pakets = Paket::where('active',1)->get();
    return view('admin.paket_tour.index',compact('pakets'));
  }
  public function formadd(Request $request){
    if ($request->action =='add') {

      $this->validate($request, [
        'nama_paket'=>'required',
        'action'=>'required',
        'min_pax'=>'required',
        'max_pax'=>'required',
        'harga'=>'required',
        'deskripsi'=>'required',
        'lokasi'=>'required',
        'gambar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
      ]);
      $hasil = explode('"',$request->lokasi);
      DB::beginTransaction();
      try {
        Paket::create([
          'nama_paket'=>$request->nama_paket,
          'min_pax'=>$request->min_pax,
          'max_pax'=>$request->max_pax,
          'harga'=>$request->harga,
          'description'=>$request->deskripsi,
          'lokasi'=>$hasil[1],
          'created_by'=>$request->user()->name,
          'active'=>1
        ]);
        $ids = Paket::where('nama_paket',$request->nama_paket)->where('pax',$request->pax)->where('harga',$request->harga)->first();
        $image = $request->file('gambar');
        $imageName = $image->getClientOriginalName();
        $fileName = date('YmdHis')."_".$imageName;
        $directory = public_path('/template/images/uploads/');
        // return $directory;
        $imageUrl = $directory.$fileName;
        Image::make($image)->resize(1920, 1280)->save($imageUrl);
        Gambar::create([
          'paket_id'=>$ids->id,
          'gambar'=>$fileName,
          'created_by'=>$request->user()->name,
          'active'=>1,
        ]);
      } catch (\Exception $e) {
        Log::info('Gagal edit logo:'.$e->getMessage());
        DB::rollback();
        return redirect()->route('admin-add-tour');
      }
      DB::commit();
      return redirect()->route('admin-paket-tour');

    }
    return view('admin.paket_tour.form_add_paket_tour');
  }
  public function detail(Request $request,$id){
    if ($request->action=='add') {
      $this->validate($request, [
        'gambar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
      ]);
      DB::beginTransaction();
      try {
        $image = $request->file('gambar');
        $imageName = $image->getClientOriginalName();
        $fileName = date('YmdHis')."_".$imageName;
        $directory = public_path('/template/images/uploads/');
        // return $directory;
        $imageUrl = $directory.$fileName;
        Image::make($image)->resize(1920, 1280)->save($imageUrl);
        Gambar::create([
          'paket_id'=>$id,
          'gambar'=>$fileName,
          'created_by'=>$request->user()->name,
          'active'=>1,
        ]);
      } catch (\Exception $e) {
        Log::info('Gagal edit logo:'.$e->getMessage());
        DB::rollback();
        return redirect()->route('admin-detail-tour',$id);
      }
      DB::commit();
      return redirect()->route('admin-detail-tour',$id);
    }elseif ($request->action=='edit') {
      $this->validate($request, [
        'gambar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
      ]);
      DB::beginTransaction();
      try {
        $gambars = Gambar::find($id);
        $destination_foto =public_path('/template/images/uploads/'.$gambars->gambar);
        if (file_exists($destination_foto)) {
            unlink($destination_foto);
        }
        $image = $request->file('gambar');
        $imageName = $image->getClientOriginalName();
        $fileName = date('YmdHis')."_".$imageName;
        $directory = public_path('/template/images/uploads/');
        $imageUrl = $directory.$fileName;
        Image::make($image)->resize(1920, 1280)->save($imageUrl);
        Gambar::find($id)->update([
          'gambar'=>$fileName,
          'updated_by'=>$request->user()->name
        ]);
      } catch (\Exception $e) {
        Log::info('Gagal edit gambar:'.$e->getMessage());
        DB::rollback();
        return redirect()->route('admin-detail-tour',$gambars->paket_id);
      }
      DB::commit();
      return redirect()->route('admin-detail-tour',$gambars->paket_id);

    }elseif ($request->action=='hapus') {
      DB::beginTransaction();
      try {
        $gambars = Gambar::find($id);
        $destination_foto =public_path('/template/images/uploads/'.$gambars->gambar);
        if (file_exists($destination_foto)) {
            unlink($destination_foto);
        }
        $gambars->deleted_by = $request->user()->name;
        $gambars->active=0;
        $gambars->update();
      } catch (\Exception $e) {
        Log::info('Gagal hapus gambar:'.$e->getMessage());
        DB::rollback();
        return redirect()->route('admin-detail-tour',$gambars->paket_id);
      }
      DB::commit();
      return redirect()->route('admin-detail-tour',$gambars->paket_id);
    }
    $pakets = Paket::find($id);
    if (Faciliti::where('paket_id',$id)->get()) {
      $facilities = Faciliti::where('paket_id',$id)->get();
    }
    return view('admin.paket_tour.detail', compact('pakets','facilities'));
  }
  public function description(Request $request,$id){
    // return $request->all();
    if ($request->action=='description') {
      $this->validate($request, [
        'deskripsi' => 'required'
      ]);
      DB::beginTransaction();
      try {
        $pakets = Paket::find($id)->update([
          'description'=>$request->deskripsi
        ]);
      } catch (\Exception $e) {
        Log::info('Gagal ganti deskripsi:'.$e->getMessage());
        DB::rollback();
        return redirect()->route('admin-detail-tour',$id);
      }
      DB::commit();
      return redirect()->route('admin-detail-tour',$id);
    }elseif ($request->action=='facilitiedit') {
      $this->validate($request, [
        'faciliti' => 'required'
      ]);
      DB::beginTransaction();
      try {
        $pakets = Faciliti::find($id);
        $pakets->name = $request->faciliti;
        $pakets->updated_by = $request->user()->name;
        $pakets->update();
      } catch (\Exception $e) {
        Log::info('Gagal ganti faciliti:'.$e->getMessage());
        DB::rollback();
        return redirect()->route('admin-detail-tour',$pakets->paket_id);
      }
      DB::commit();
      return redirect()->route('admin-detail-tour',$pakets->paket_id);
    }elseif ($request->action=='facilititambah') {
      $this->validate($request, [
        'faciliti' => 'required'
      ]);
      DB::beginTransaction();
      try {
        Faciliti::create([
          'name'=>$request->faciliti,
          'paket_id'=>$id,
          'active'=>1,
          'created_by'=>$request->user()->name
        ]);
      } catch (\Exception $e) {
        Log::info('Gagal ganti faciliti:'.$e->getMessage());
        DB::rollback();
        return redirect()->route('admin-detail-tour',$id);
      }
      DB::commit();
      return redirect()->route('admin-detail-tour',$id);
    }

  }
  public function editepax(Request $request,$id){
    $pakets = Paket::find($id);
    $pakets->min_pax = $request->min_pax;
    $pakets->max_pax = $request->max_pax;
    if ($pakets->update()) {
      return redirect()->back();
    }
    return redirect()->back();
  }

}
