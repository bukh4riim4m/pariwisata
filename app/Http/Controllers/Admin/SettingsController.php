<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Slider;
use App\Web;
use App\Blok;
use DB;
use Log;
use Session;
use Image;

class SettingsController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
  public function logo(Request $request){
    $this->validate($request, [
      'title'=>'required|max:30',
      'gambar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
    ]);
    DB::beginTransaction();
    try {
      if ($request->file('gambar')) {
        $gambars = Web::find(1);
        $destination_foto =public_path('/template/images/txt/'.$gambars->logo);
        if (file_exists($destination_foto)) {
            unlink($destination_foto);
        }
        $image = $request->file('gambar');
        $imageName = $image->getClientOriginalName();
        $fileName = date('YmdHis')."_".$imageName;
        $directory = public_path('/template/images/txt/');
        $imageUrl = $directory.$fileName;
        // return $imageUrl;
        Image::make($image)->resize(224, 50)->save($imageUrl);
        $gambars->logo = $fileName;
        $gambars->title_web = $request->title;
        $gambars->update();
      }else {
        $gambars = Web::find(1);
        $gambars->title_web = $request->title;
        $gambars->update();
      }
    } catch (\Exception $e) {
      Log::info('Gagal edit logo:'.$e->getMessage());
      DB::rollback();
      return redirect()->route('admin-settings');
    }
    DB::commit();
    return redirect()->route('admin-settings');
  }
  public function index(Request $request){

    if ($request->action =='edit') {
      $this->validate($request, [
        'title'=>'required',
        'deskripsi'=>'required'
      ]);
      DB::beginTransaction();
      try {
        if ($request->file('gambar')) {
          $this->validate($request, [
            'title'=>'required',
            'deskripsi'=>'required',
            'gambar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
          ]);
          $gambars = Slider::find($request->ids);
          $destination_foto =public_path('/template/images/slider/'.$gambars->gambar);
          if (file_exists($destination_foto)) {
              unlink($destination_foto);
          }
          $image = $request->file('gambar');
          $imageName = $image->getClientOriginalName();
          $fileName = date('YmdHis')."_".$imageName;
          $directory = public_path('/template/images/slider/');

          $imageUrl = $directory.$fileName;
          // return $imageUrl;
          Image::make($image)->resize(1920, 1280)->save($imageUrl);
        }

        $sliders = Slider::find($request->ids)->update([
          'gambar'=>$fileName,
          'title'=>$request->title,
          'description'=>$request->deskripsi
        ]);
      } catch (\Exception $e) {
        Log::info('Gagal Tambah Slide:'.$e->getMessage());
        DB::rollback();
        // flash()->overlay("<p class='alert alert-danger'>Transaksi GAGAL.</p>", "INFO");
        return redirect()->back();
      }
      DB::commit();
      return redirect()->route('admin-settings');
    }elseif ($request->action =='add') {

      $this->validate($request, [
        'title'=>'required',
        'deskripsi'=>'required',
        'gambar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
      ]);
      // return $request->all();
      DB::beginTransaction();
      try {
        $image = $request->file('gambar');
        $imageName = $image->getClientOriginalName();
        $fileName = date('YmdHis')."_".$imageName;
        $directory = public_path('/template/images/slider/');
        // return $directory;
        $imageUrl = $directory.$fileName;
        Image::make($image)->resize(1920, 1280)->save($imageUrl);

        $sliders = Slider::create([
          'gambar'=>$fileName,
          'title'=>$request->title,
          'description'=>$request->deskripsi
        ]);
      } catch (\Exception $e) {
        Log::info('Gagal Tambah Slide:'.$e->getMessage());
        DB::rollback();
        // flash()->overlay("GAGAL", "INFO");
        return redirect()->back();
      }
      DB::commit();
      // flash()->overlay("BERHASIL", "INFO");
      return redirect()->route('admin-settings');
    }elseif ($request->action =='hapus') {
      $gambars = Slider::find($request->ids);
      $destination_foto =public_path('/template/images/slider/'.$gambars->gambar);
      if (file_exists($destination_foto)) {
          unlink($destination_foto);
      }
      $gambars->delete();
    }
    $sliders = Slider::get();
    $abouts = Blok::where('active',1)->where('page','about')->get();
    $faqs= Blok::where('active',1)->where('page','faq')->get();
    $paymenoptions= Blok::where('active',1)->where('page','payment')->get();
    $bookingtips= Blok::where('active',1)->where('page','tips')->get();
    $reservations= Blok::where('active',1)->where('page','reservation')->get();
    return view('admin.setting',compact('sliders','abouts','faqs','bookingtips','paymenoptions','reservations'));
  }
  public function blog(Request $request){

    if ($request->action == 'add') {
      $this->validate($request, [
        'title'=>'required',
        'page'=>'required',
        'description'=>'required'
      ]);
      DB::beginTransaction();
      try {
        Blok::create([
          'title'=>$request->title,
          'description'=>$request->description,
          'page'=>$request->page,
          'active'=>1,
          'created_by'=>$request->user()->name
        ]);
      } catch (\Exception $e) {
        Log::info('Gagal Tambah blog:'.$e->getMessage());
        DB::rollback();
        return redirect()->back();
      }
      DB::commit();
      return redirect()->back();
    }
  }
  public function editblog(Request $request,$id){
    $blogs = Blok::find($id);
    if ($request->action =='simpan') {
      $blogs->title = $request->title;
      $blogs->description = $request->deskripsi;
      $blogs->updated_by = $request->user()->name;
      if ($blogs->update()) {
        return redirect()->route('admin-settings');
      }
    }elseif ($request->action =='hapus') {
      $blogs->active = 0;
      if ($blogs->update()) {
        return redirect()->route('admin-settings');
      }
    }
    return view('admin.edit_blog',compact('blogs'));
  }
  public function contact(Request $request){
    $this->validate($request, [
      'telp'=>'required',
      'hp'=>'required',
      'email'=>'required',
      'maps'=>'required'
    ]);

    $lokasi = explode('"',$request->maps);

    // return dd($lokasi);
    DB::beginTransaction();
    try {
      Web::find(1)->update([
        'telp'=>$request->telp,
        'hp'=>$request->hp,
        'email'=>$request->email,
        'maps'=>$lokasi[1],
      ]);
    } catch (\Exception $e) {
      Log::info('Gagal edit contact:'.$e->getMessage());
      DB::rollback();
      return redirect()->back();
    }
    DB::commit();
    return redirect()->back();
  }
}
