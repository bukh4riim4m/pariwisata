<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Blok;
use DB;
use Log;

class AdminController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }

  public function index()
  {
      return view('admin.index');
  }
  public function pageblog(Request $request){
    $blogs = Blok::where('active',1)->where('page','blog')->orderBy('id','DESC')->paginate(10);
    return view('admin.blogs.index',compact('blogs'));
  }
  public function addblog(Request $request){
    if ($request->action =='add') {
      $this->validate($request, [
        'title'=>'required',
        'deskripsi'=>'required',
      ]);
      DB::beginTransaction();
      try {
        Blok::create([
          'title'=>$request->title,
          'description'=>$request->deskripsi,
          'active'=>1,
          'page'=>'blog',
          'created_by'=>$request->user()->name
        ]);
      } catch (\Exception $e) {
        Log::info('Gagal ganti faciliti:'.$e->getMessage());
        DB::rollback();
        return redirect()->back();
      }
      DB::commit();
      return redirect()->route('admin-blog');
    }
    return view('admin.blogs.form_add_blog');
  }

  public function editeblog(Request $request,$id){
    if ($request->action == 'edit') {
      $this->validate($request, [
        'title'=>'required',
        'deskripsi'=>'required',
      ]);
      DB::beginTransaction();
      try {
        Blok::find($id)->update([
          'title'=>$request->title,
          'description'=>$request->deskripsi,
          'updated_by'=>$request->user()->name
        ]);
      } catch (\Exception $e) {
        Log::info('Gagal ganti blog:'.$e->getMessage());
        DB::rollback();
        return redirect()->back();
      }
      DB::commit();
      return redirect()->route('admin-blog');
    }elseif ($request->action =='hapus') {
      DB::beginTransaction();
      try {
        Blok::find($id)->update([
          'active'=>0,
          'deleted_by'=>$request->user()->name
        ]);
      } catch (\Exception $e) {
        Log::info('Gagal ganti blog:'.$e->getMessage());
        DB::rollback();
        return redirect()->back();
      }
      DB::commit();
      return redirect()->route('admin-blog');
    }
    $blogs = Blok::find($id);
    return view('admin.blogs.edit_blog',compact('blogs'));
  }
  

}
