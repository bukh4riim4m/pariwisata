<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Gallery;
use Image;
use DB;
use Log;

class GalleryController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
    public function gallery(Request $request){
      if ($request->action == 'editinduk') {
        $this->validate($request, [
          'title'=>'required|max:30',
          'gambar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        DB::beginTransaction();
        try {
          if ($request->file('gambar')) {
            $gambars = Gallery::find($request->ids);
            $destination_foto =public_path('/template/images/gallery/'.$gambars->img_gallery);
            if (file_exists($destination_foto)) {
                unlink($destination_foto);
            }
            $image = $request->file('gambar');
            $imageName = $request->title.'.png';
            $fileName = date('YmdHis')."_".$imageName;
            $directory = public_path('/template/images/gallery/');
            $imageUrl = $directory.$fileName;
            // return $imageUrl;
            Image::make($image)->resize(1920, 1280)->save($imageUrl);
            $gambars->img_gallery = $fileName;
            $gambars->title = $request->title;
            $gambars->update();
          }else {
            $gambars = Gallery::find($request->ids);
            $gambars->title = $request->title;
            $gambars->update();
          }
        } catch (\Exception $e) {
          Log::info('Gagal edit logo:'.$e->getMessage());
          DB::rollback();
          return redirect()->route('admin-gallery');
        }
        DB::commit();
        return redirect()->route('admin-gallery');
      }elseif ($request->action == 'addinduk') {
        $this->validate($request, [
          'title'=>'required|max:30',
          'gambar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        DB::beginTransaction();
        try {
          $image = $request->file('gambar');
          $imageName = $request->title.'.png';
          $fileName = date('YmdHis')."_".$imageName;
          Gallery::create([
            'utama_id'=>0,
            'title'=> $request->title,
            'img_gallery'=> $fileName,
            'status'=> 'induk',
            'created_by'=> $request->user()->name,
          ]);
          $directory = public_path('/template/images/gallery/');
          $imageUrl = $directory.$fileName;
          Image::make($image)->resize(1920, 1280)->save($imageUrl);

        } catch (\Exception $e) {
          Log::info('Gagal tambah induk gallery:'.$e->getMessage());
          DB::rollback();
          return redirect()->route('admin-gallery');
        }
        DB::commit();
        return redirect()->route('admin-gallery');
      }
      $galleries = Gallery::where('status','induk')->get();
      return view('admin.gallery.index',compact('galleries'));
    }
    public function anakgallery(Request $request,$id){
      $id_induk = $id;
      if ($request->action == 'add') {
        $this->validate($request, [
          'gambar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        DB::beginTransaction();
        try {
          $image = $request->file('gambar');
          $imageName = $request->title.'.png';
          $fileName = date('YmdHis')."_".$imageName;
          Gallery::create([
            'utama_id'=>$id,
            'title'=> 'kosong',
            'img_gallery'=> $fileName,
            'status'=> 'anak',
            'created_by'=> $request->user()->name,
          ]);
          $directory = public_path('/template/images/gallery/');
          $imageUrl = $directory.$fileName;
          Image::make($image)->resize(1920, 1280)->save($imageUrl);

        } catch (\Exception $e) {
          Log::info('Gagal tambah induk gallery:'.$e->getMessage());
          DB::rollback();
          return redirect()->route('admin-gallery-anak',$id_induk);
        }
        DB::commit();
        // return redirect()->route('admin-gallery-anak',$id_induk);
      }
      $title = Gallery::find($id);
      $galleries = Gallery::where('utama_id',$id)->get();
      return view('admin.gallery.anak_gallery',compact('galleries','id_induk','title'));
    }
}
