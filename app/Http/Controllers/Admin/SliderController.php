<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Slider;
use DB;
use Log;

class SliderController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
  public function index(){
    return view('admin.slider');
  }
  public function create(Request $request){
    if ($request->action =='create') {
      DB::beginTransaction();
      try {
        $datas = Slider::create([
          'gambar'=>$request->gambar,
          'title'=>$request->title,
          'description'=>$request->description
        ]);
      } catch (\Exception $e) {
        Log::info('Gagal Tambah Slide:'.$e->getMessage());
        DB::rollback();
        flash()->overlay("<p class='alert alert-danger'>Transaksi GAGAL.</p>", "INFO");
        return redirect()->back();
      }
      DB::commit();
      return redirect()->route('admin-slider');
    }

    return view('admin.create_slider');
  }
}
