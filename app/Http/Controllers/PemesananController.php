<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pemesanan;
use App\Pessenger;
use App\User;
use App\Paket;
use DB;
use Log;
use PDF;
use Illuminate\Support\Facades\App;

class PemesananController extends Controller
{
    public function pemesan(Request $request){
      // return $request->all();
      if ($request->action =='add') {
        $this->validate($request, [
          'name'=>'required',
          'hp'=>'required',
          'nama_pemesan'=>'required',
          'telp'=>'required',
          'email'=>'required',
          'rekening'=>'required',
          'action'=>'required',
        ]);
        $pakets = Paket::find($request->ids);
        $total = $pakets->harga * $request->jumlah_pax;
        $pessenger = date('ymdhis').rand(1,100);
        $name = $request->name;
        $hp = $request->hp;
        DB::beginTransaction();
        try {
          Pemesanan::create([
            'nama_pemesan'=>$request->nama_pemesan,
            'email_pemesan'=>$request->email,
            'telp'=>$request->telp,
            'pessenger_key'=>$pessenger,
            'paket_id'=>$pakets->id,
            'invoice'=>date('ymdhis'),
            'jumlah_pax'=>$request->jumlah_pax,
            'total_harga'=>$total,
            'kode_unik'=>rand(100,999),
            'bank_id'=>1,
            'status'=>'Pending',
          ]);
          for ($i=0; $i < (int)$request->jumlah_pax; $i++) {
            Pessenger::create([
              'pemesanans_id'=>$pessenger,
              'nama'=>$name[$i],
              'hp'=>$hp[$i],
            ]);
          }

        } catch (\Exception $e) {
          Log::info('Gagal input pemesan:'.$e->getMessage());
          DB::rollback();
          return redirect()->back();
        }
        DB::commit();
        $pemesanans = Pemesanan::where('pessenger_key',$pessenger)->first();
        $pessengers = Pessenger::where('pemesanans_id',$pemesanans->pessenger_key)->get();
        return view('informasi_transfer', compact('pemesanans','pessengers'));
      }
    }
    public function download(Request $request){
      $pemesanans = Pemesanan::where('pessenger_key',$request->pessenger_key)->first();
      $pessengers = Pessenger::where('pemesanans_id',$pemesanans->pessenger_key)->get();
      $pdf = App::make('dompdf.wrapper');
      $pdf = PDF::loadView('reports._invoice',['pemesanans' => $pemesanans,'pessengers'=>$pessengers]);
      return $pdf->download('invoice.pdf');
    }
}
