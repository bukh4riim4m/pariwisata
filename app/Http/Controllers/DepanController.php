<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JavaScript;
use App\Paket;
use App\Blok;
use App\Gallery;
use App\Faciliti;

class DepanController extends Controller
{
    public function index(){
      $pakets = Paket::where('active',1)->get();
      return view('index', compact('pakets'));
    }
    public function tours(Request $request){
      $pakets = Paket::where('active',1)->get();
      return view('tours',compact('pakets'));
    }
    public function detail(Request $request,$id){
      $pakets = Paket::find($id);
      $facilities = [];
      if (Faciliti::where('paket_id',$id)->get()) {
        $facilities = Faciliti::where('paket_id',$id)->get();
      }
      return view('detail', compact('pakets','facilities'));
    }
    public function logout(Request $request){
      $request->session()->invalidate();
      return redirect()->route('index');
    }
    public function contact(Request $request){
      return view('contact');
    }
    public function gallery(Request $request){
      $galleries = Gallery::where('status','induk')->get();
      return view('gallery',compact('galleries'));
    }
    public function gallerydetail($id){
      $title = Gallery::find($id);
      $galleries = Gallery::where('utama_id',$id)->get();
      return view('detail_gallery',compact('galleries','id_induk','title'));
    }
    public function blog(Request $request){
      $blogs = Blok::where('active',1)->where('page','blog')->orderBy('id','DESC')->paginate(10);
      return view('blog',compact('blogs'));
    }
    public function blogdetail($id){
      $blogs = Blok::find($id);
      return view('detail_blog',compact('blogs'));
    }
    public function about(Request $request){
      $blogs = Blok::where('active',1)->where('page','about')->get();
      return view('about',compact('blogs'));
    }
    public function faq(){
      $blogs = Blok::where('active',1)->where('page','faq')->get();
      return view('faq',compact('blogs'));
    }
    public function reservation(){
      $blogs = Blok::where('active',1)->where('page','reservation')->get();
      return view('panduan_reservasi',compact('blogs'));
    }
    public function payment(){
      $blogs = Blok::where('active',1)->where('page','payment')->get();
      return view('payment_option',compact('blogs'));
    }
    public function booking(){
      $blogs = Blok::where('active',1)->where('page','tips')->get();
      return view('booking_tips',compact('blogs'));
    }
    public function komentar(Request $request,$id){
      return redirect()->back();
    }
    public function pesenger(Request $request){
      $ids = $request->ids;
      $forms = $request->jumlah_pax;
      return view('form_nama',compact('forms','ids'));
    }
    public function tes(){
      $pemesanans = \App\Pemesanan::find(1);
      $pessengers = \App\Pessenger::where('pemesanans_id',$pemesanans->pessenger_key)->get();
      return view('informasi_transfer', compact('pemesanans','pessengers'));
    }
}
