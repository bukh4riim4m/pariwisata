<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Slider;
use App\Blok;
use App\Wisatabuatan;
use App\GambarWisataBuatan;
use Auth;
use Carbon;

class DepanController extends Controller
{
    public function slider(Request $request){
      $sliders = Slider::get();
      $gambar = array();
      $url = route('index');
      foreach ($sliders as $key => $slider) {
        $gambar[] = $url.'/template/images/slider/'.$slider->gambar;
      }
      return response()->json($gambar);
    }

    public function login(Request $request){
      $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
            return response()->json([
              'code'=>'401',
              'status'=>'error',
                'message' => 'email atau Password'
            ]);
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        if ($token->save()) {
          $response = [
            'code'=>'200',
            'status'=>'OK',
            'message'=>'berhasil',
            'user'=>$user
            // 'token'=>$tokenResult
          ];
        }
        return response()->json($response);
        // return response()->json([
        //     'access_token' => $tokenResult->accessToken,
        //     'token_type' => 'Bearer',
        //     'expires_at' => Carbon::parse(
        //         $tokenResult->token->expires_at
        //     )->toDateTimeString()
        // ]);
    }
    public function blogs(Request $request){
      $blok = Blok::where('active',1)->where('page','blog')->get();
      $respon = [
        'code'=>200,
        'status'=>'berhasil',
        'data'=>$blok
      ];
      return response()->json($respon);
    }
    public function wisata(Request $request){
      $data = Wisatabuatan::where('is_active',1)->where('category',$request->category)->get();
      $respon = [
        'code'=>200,
        'status'=>'berhasil',
        'data_list_wisata'=>$data
      ];
      return response()->json($respon);
    }
}
