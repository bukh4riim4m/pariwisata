@extends('layouts.umum_app')

@section('content')
<div class="main" role="main">
  <div class="wrap clearfix">
    <!--main content-->
    <div class="content clearfix">
      <!--breadcrumbs-->
      <nav role="navigation" class="breadcrumbs clearfix">
        <!--crumbs-->
        <ul class="crumbs">
          <li><a href="{{route('index')}}" title="Dashboard">Dashboard</a></li>
          <li><a href="{{route('gallery')}}" title="Gallery">Gallery</a></li>
          <li>{{$title->title}}</li>
        </ul>
        <!--//crumbs-->
      </nav>

      <!--three-fourth content-->
      <section class="full">
        <div class="deals clearfix">

          <article class="one-fourth">
  					<figure class="left"><img width="270" height="152" src="{{asset('template/images/gallery/'.$title->img_gallery)}}" alt="" width="270" height="152" /><a href="{{asset('template/images/gallery/'.$title->img_gallery)}}" class="image-overlay" rel="prettyPhoto[gallery1]"></a></figure>
  				</article>
          @foreach($galleries as $key => $gallery)
          <article class="one-fourth">
            <figure class="left"><img width="270" height="152" src="{{asset('template/images/gallery/'.$gallery->img_gallery)}}" alt="" width="270" height="152" /><a href="{{asset('template/images/gallery/'.$gallery->img_gallery)}}" class="image-overlay" rel="prettyPhoto[gallery1]"></a></figure>
  					<!-- <figure><a href="#" title=""><img src="{{asset('template/images/gallery/'.$gallery->img_gallery)}}" alt="" width="270" height="152" /></a></figure> -->
  				</article>
          @endforeach
          <!--//item-->
</div>
        <!--//get inspired list-->
      </section>

      <!--three-fourth content-->
    </div>
    <!--//main content-->
  </div>
</div>
@endsection
