@extends('layouts.umum_app')
<style>
#customers {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    font-size: 13px;
    width: 100%;
}

#customers td, #customers th {
    border: 1px solid #ddd;
    padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

/* #customers tr:hover {background-color: #ddd;} */

#customers th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #41AFAA;
    color: white;
}
</style>
@section('content')
<div class="main" role="main">
  <div class="wrap clearfix">
    <!--main content-->
    <div class="content clearfix">
      <!--breadcrumbs-->
      <nav role="navigation" class="breadcrumbs clearfix">
        <!--crumbs-->
        <ul class="crumbs">
          <li><a href="{{route('index')}}" title="Dashboard">Dashboard</a></li>
          <li><a href="{{route('tours')}}" title="Paket Tour">Tours</a></li>
          <li><a href="#" title="Informasi Transfer">Informasi Transfer</a></li>
        </ul>
        <ul class="top-right-nav">
					<!-- <li><a href="#" title="Back to results"><button  type="button" class="gradient-button" name="button">TAMBAH</button></a></li> -->
					<li><a href="{{route('tours')}}" title="Back">Back</a></li>
				</ul>
        <!--//crumbs-->

      </nav>
      <!--three-fourth content-->
        <section class="three-fourth">
          <div class="booking">
            <fieldset>
              <h3>PACKAGE : <span>{{$pemesanans->paketId->nama_paket}} </span></h3>
              <div class="row triplets">
                <div class="f-item">
                  <figure><a href="" title=""><img src="{{asset('template/images/uploads/'.$pemesanans->paketId->gambar->gambar)}}" alt="" width="270" height="152" /></a></figure>
                </div>
                <div class="f-item">
                  <table id="customers">
                    <tr>
                      <td colspan="2"><h5>{{$pemesanans->paketId->day}}</h5> </td>
                    </tr>
                    <tr>
                      <td>Nama Pemesan </td><td> : {{$pemesanans->nama_pemesan}}</td>
                    </tr>
                    <tr>
                      <td>Jumlah Pax </td><td> : {{$pemesanans->jumlah_pax}}</td>
                    </tr>
                    <tr>
                      <td>Harga / Pax </td><td> : IDR {{number_format($pemesanans->total_harga / $pemesanans->jumlah_pax,0,",",".")}}</td>
                    </tr>
                  </table>
                </div>
              </div>

              <hr />
              <h2>
                <span>DATA </span>PAX</h2>
              <div class="row b-info">
                <table class="table" id="customers">
                  <tr>
                    <th style="max-width:2px;">No.</th>
                    <th>Nama</th>
                    <th>No. Telp</th>
                  </tr>
                  @foreach($pessengers as $key => $pessenger)
                  <tr>
                    <td>{{$key+1}}.</td><td>{{$pessenger->nama}}</td><td>{{$pessenger->hp}}</td>
                  </tr>
                  @endforeach
                </table>
              </div>

              <hr />
              <h2>INFO TRANSFER</h2>
              <div class="row triplets">
                <div class="f-item">
                  <label for="card_number">Rekening Tujuan</label>
                  <img src="{{asset('template/images/txt/'.$pemesanans->bankId->logo)}}" alt="" width="100px;">
<br>
                  <p>{{$pemesanans->bankId->nama_bank}}<br>
                    No Rek : {{$pemesanans->bankId->rekening}}<br>
                    A/N : {{$pemesanans->bankId->atas_nama}}
                  </p>
                </div>
                <div class="f-item">
                  <table id="customers">
                    <tr>
                      <td>#INVOICE </td>
                      <td>: {{$pemesanans->invoice}}</td>
                    </tr>
                    <tr>
                      <td>TOTAL HARGA </td>
                      <td>: IDR {{number_format($pemesanans->total_harga,0,",",".")}}</td>
                    </tr>
                    <tr>
                      <td>KODE UNIK </td>
                      <td>: {{$pemesanans->kode_unik}}</td>
                    </tr>
                    <tr>
                      <td><em>TOTAL TRANSFER</em> </td>
                      <td>:<em> IDR {{number_format($pemesanans->total_harga+$pemesanans->kode_unik,0,",",".")}}</em></td>
                    </tr>
                  </table>

                </div>
              </div>
              <hr />
              <form class="" action="{{route('download-invoice')}}" method="post">
                @csrf
                <input type="hidden" name="pessenger_key" value="{{$pemesanans->pessenger_key}}" required>
                <input type="submit" class="gradient-button" value="Download" id="next-step" />
                <a href="#note" class="gradient-button" style="background:green;">NOTE</a>
              </form>


            </fieldset>
          </div>
        </section>
      <!--//three-fourth content-->

      <!--right sidebar-->
      @include('includes.right_sidebar')
      <!--//right sidebar-->
    </div>
  </div>
</div>
@endsection
