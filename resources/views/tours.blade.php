@extends('layouts.umum_app')

@section('content')
<div class="main" role="main">
  <div class="wrap clearfix">
    <!--main content-->
    <div class="content clearfix">
      <!--breadcrumbs-->
      <nav role="navigation" class="breadcrumbs clearfix">
        <!--crumbs-->
        <ul class="crumbs">
          <li><a href="{{route('index')}}" title="Dashboard">Dashboard</a></li>
          <li>Paket Tours</li>
        </ul>
        <!--//crumbs-->
      </nav>

      <section class="three-fourth">
				<!-- <h1>Paket Tour</h1> -->
				<div class="deals clearfix">
					<!--deal-->
					@foreach($pakets as $key => $paket)
					<?php $gambars = App\Gambar::where('paket_id',$paket->id)->first(); ?>
					<article class="one-fourth">
						<figure><a href="{{route('detail-tour',$paket->id)}}" title=""><img src="{{asset('template/images/uploads/'.$gambars->gambar)}}" alt="" width="270" height="152" /></a></figure>
						<div class="details">
							<h1>{{ucwords(strtolower($paket->nama_paket))}}
								<span class="stars">
									<img src="{{asset('template/images/ico/star.png')}}" alt="" />
									<img src="{{asset('template/images/ico/star.png')}}" alt="" />
									<img src="{{asset('template/images/ico/star.png')}}" alt="" />
									<img src="{{asset('template/images/ico/star.png')}}" alt="" />
								</span>
							</h1>
							<span class="address">Indonesia  • </span>
							<span class="rating"> 9 /10</span>
              <hr>
              <span class="address">Jumlah Pax  <em class="right"> {{$paket->min_pax}} - {{$paket->max_pax}} Pax</em> </span>
							<span class="price">Harga / Pax  <em> IDR {{number_format($paket->harga,0,",",".")}}</em> </span>
							<!-- <div class="description">
								<p>{{$paket->description}} <a href="hotel.html">More info</a></p>
							</div> -->
							<a href="{{route('detail-tour',$paket->id)}}" title="Detail Paket Tour" class="gradient-button">Detail</a>
						</div>
					</article>
					@endforeach
					<!--//deal-->
				</div>
			</section>
      <aside class="right-sidebar">
        <!--hotel details-->


        <!--Need Help Booking?-->
        <article class="default clearfix">
          <h2>Need Help Booking?</h2>
          <p>Call our customer services team on the number below to speak to one of our advisors who will help you with all of your holiday needs.</p>
          <?php $webs = App\Web::find(1); ?>
          <p class="phone-green">{{$webs->telp}}</p>
          <p class="phone-green">{{$webs->hp}}</p>
          <p class="email-green"><a href="#">{{$webs->email}}</a></p>
        </article>
        <!--//Need Help Booking?-->

        <!--Why Book with us?-->
        <article class="default clearfix">
          <h2>Why Book with us?</h2>
          <h3>Low rates</h3>
          <p>Get the best rates, or get a refund.<br />No booking fees. Save money!</p>
          <h3>Largest Selection</h3>
          <p>140,000+ hotels worldwide<br />130+ airlines<br />Over 3 million guest reviews</p>
          <h3>We’re Always Here</h3>
          <p>Call or email us, anytime<br />Get 24-hour support before, during, and after your trip</p>
        </article>
        <!--//Why Book with us?-->

      </aside>
    </div>
  </div>
</div>
@endsection
