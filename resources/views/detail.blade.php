@extends('layouts.umum_app')

@section('content')

<div class="main" role="main">
  <div class="wrap clearfix">
    <!--main content-->
    <div class="content clearfix">
      <!--breadcrumbs-->
      <nav role="navigation" class="breadcrumbs clearfix">
        <!--crumbs-->
        <ul class="crumbs">
          <li><a href="{{route('index')}}" title="Dashboard">Dashboard</a></li>
          <li><a href="{{route('tours')}}" title="Tours">Tours</a></li>
          <li><a href="#" title="Detail">{{$pakets->nama_paket}}</a></li>
        </ul>
        <ul class="top-right-nav">
					<!-- <li><a href="search_results.html" title="Back to results"><button  type="button" class="gradient-button" name="button">TAMBAH</button></a></li> -->
					<li> <strong><a href="{{route('admin-slider')}}" title="Back to results">Kembali</a></strong> </li>
				</ul>
        <!--//crumbs-->
      </nav>
      <section class="three-fourth">
        <div class="gallery">
						<div class="lSSlideOuter ">
              <div class="lSSlideWrapper usingCss" style="transition-duration: 500ms; transition-timing-function: ease;">
                <ul id="image-gallery" class="lightSlider lsGrab lSSlide" style="width: 6980px; transform: translate3d(-3490px, 0px, 0px); height: 581.656px; padding-bottom: 0%;">
                  <?php $gambars = App\Gambar::where('paket_id',$pakets->id)->where('active',1)->get(); ?>
                  @foreach($gambars as $key => $gambar)
                  <li data-thumb="{{asset('template/images/uploads/'.$gambar->gambar)}}" class="lslide" style="width: 872.5px; margin-right: 0px;">
    								<img src="{{asset('template/images/uploads/'.$gambar->gambar)}}" alt="">
    							</li>
                  @endforeach
                </ul>
              <div class="lSAction">
                <a class="lSPrev"></a><a class="lSNext"></a>
              </div>
            </div>
                <!-- <ul class="lSPager lSGallery" style="margin-top: 5px; transition-duration: 500ms; width: 878px; transform: translate3d(-0.5px, 0px, 0px);">
                  <li style="width:100%;width:141.25px;margin-right:5px" class="">
                    <a href="#"><img src="{{asset('template/images/uploads/hotel1.jpg')}}"></a>
                  </li>
                  <li style="width:100%;width:141.25px;margin-right:5px">
                    <a href="#"><img src="{{asset('template/images/uploads/hotel1.jpg')}}"></a>
                  </li>
                  <li style="width:100%;width:141.25px;margin-right:5px">
                    <a href="#"><img src="{{asset('template/images/uploads/hotel1.jpg')}}"></a>
                  </li>
                  <li style="width:100%;width:141.25px;margin-right:5px" class="active">
                    <a href="#"><img src="{{asset('template/images/uploads/hotel1.jpg')}}"></a>
                  </li>
                  <li style="width:100%;width:141.25px;margin-right:5px">
                    <a href="#"><img src="{{asset('template/images/uploads/hotel1.jpg')}}"></a>
                  </li>
                  <li style="width:100%;width:141.25px;margin-right:5px">
                    <a href="#"><img src="{{asset('template/images/uploads/hotel1.jpg')}}"></a>
                  </li>
                </ul> -->
              </div>
					</div>
        <!--gallery-->
        <!-- <section class="gallery" id="crossfade">
          <img src="{{asset('template/images/slider/img.jpg')}}" alt="" width="850" height="531" />
          <img src="{{asset('template/images/slider/img.jpg')}}" alt="" width="850" height="531" />
          <img src="{{asset('template/images/slider/img.jpg')}}" alt="" width="850" height="531" />
          <img src="{{asset('template/images/slider/img.jpg')}}" alt="" width="850" height="531" />
        </section> -->
        <!--//gallery-->

        <!--inner navigation-->
        <nav class="inner-nav">
          <ul>
            <!-- <li class="availability"><a href="#availability" title="Availability">Availability</a></li> -->
            <li class="description"><a href="#description" title="Description">Description</a></li>
            <li class="facilities"><a href="#facilities" title="Facilities">Facilities</a></li>
            <li class="location"><a href="#location" title="Location">Location</a></li>
            <!-- <li class="reviews"><a href="#reviews" title="Book Now">Book Now</a></li> -->
            <!-- <li class="things-to-do"><a href="#things-to-do" title="Things to do">Things to do</a></li> -->
          </ul>
        </nav>
        <!--//inner navigation-->

        <!--availability-->
        {{--<section id="availability" class="tab-content">
          <article>
            <h1>Availability</h1>
            <div class="text-wrap">
              <a href="#" class="gradient-button right" title="Change dates">Change dates</a>
              <p>Available rooms from <span class="date">Thurs 29 Nov 2012</span> to <span class="date">Fri 30 Nov 2012</span>.</p>
            </div>

            <h1>Room types</h1>
            <ul class="room-types">
              <!--room-->
              <li>
                <figure class="left"><img src="{{asset('template/images/slider/img.jpg')}}" alt="" width="270" height="152" /><a href="{{asset('template/images/slider/img.jpg')}}" class="image-overlay" rel="prettyPhoto[gallery1]"></a></figure>
                <div class="meta">
                  <h2>Superior Double Room</h2>
                  <p>Prices are per room<br />20 % VAT Included in price</p>
                  <p>Non-refundable<br />Full English breakfast $ 24.80 </p>
                  <a href="javascript:void(0)" title="more info" class="more-info">+ more info</a>
                </div>
                <div class="room-information">
                  <div class="row">
                    <span class="first">Max:</span>
                    <span class="second"><img src="{{asset('template/images/ico/person.png')}}" alt="" /><img src="{{asset('template/images/ico/person.png')}}" alt="" /></span>
                  </div>
                  <div class="row">
                    <span class="first">Price:</span>
                    <span class="second">$ 55</span>
                  </div>
                  <div class="row">
                    <span class="first">Rooms:</span>
                    <span class="second">01</span>
                  </div>
                  <a href="booking-step1.html" class="gradient-button" title="Book">Book</a>
                </div>
                <div class="more-information">
                  <p>Stylish and individually designed room featuring a satellite TV, mini bar and a 24-hour room service menu.</p>
                  <p><strong>Room Facilities:</strong> Safety Deposit Box, Air Conditioning, Desk, Ironing Facilities, Seating Area, Heating, Shower, Bath, Hairdryer, Toilet, Bathroom, Pay-per-view Channels, TV, Telephone</p>
                  <p><strong>Bed Size(s):</strong> 1 Double </p>
                  <p><strong>Room Size:</strong>  16 square metres</p>
                </div>
              </li>
              <!--//room-->

              <!--room-->
              <li>
                <figure class="left"><img src="{{asset('template/images/slider/img.jpg')}}" alt="" width="270" height="152" /><a href="{{asset('template/images/slider/img.jpg')}}" class="image-overlay" rel="prettyPhoto[gallery1]"></a></figure>
                <div class="meta">
                  <h2>Deluxe Single Room</h2>
                  <p>Prices are per room<br />20 % VAT Included in price</p>
                  <p>Non-refundable<br />Full English breakfast $ 24.80 </p>
                  <a href="javascript:void(0)" title="more info" class="more-info">+ more info</a>
                </div>
                <div class="room-information">
                  <div class="row">
                    <span class="first">Max:</span>
                    <span class="second"><img src="{{asset('template/images/ico/person.png')}}" alt="" /></span>
                  </div>
                  <div class="row">
                    <span class="first">Price:</span>
                    <span class="second">$ 55</span>
                  </div>
                  <div class="row">
                    <span class="first">Rooms:</span>
                    <span class="second">01</span>
                  </div>
                  <a href="booking-step1.html" class="gradient-button" title="Book">Book</a>
                </div>
                <div class="more-information">
                  <p>Stylish and individually designed room featuring a satellite TV, mini bar and a 24-hour room service menu.</p>
                  <p><strong>Room Facilities:</strong> Safety Deposit Box, Air Conditioning, Desk, Ironing Facilities, Seating Area, Heating, Shower, Bath, Hairdryer, Toilet, Bathroom, Pay-per-view Channels, TV, Telephone</p>
                  <p><strong>Bed Size(s):</strong> 1 Double </p>
                  <p><strong>Room Size:</strong>  16 square metres</p>
                </div>
              </li>
              <!--//room-->

              <!--room-->
              <li>
                <figure class="left"><img src="{{asset('template/images/slider/img.jpg')}}" alt="" width="270" height="152" /><a href="{{asset('template/images/slider/img.jpg')}}" class="image-overlay" rel="prettyPhoto[gallery1]"></a></figure>
                <div class="meta">
                  <h2>Standard Family Room</h2>
                  <p>Prices are per room<br />20 % VAT Included in price</p>
                  <p>Non-refundable<br />Full English breakfast $ 24.80 </p>
                  <a href="javascript:void(0)" title="more info" class="more-info">+ more info</a>
                </div>
                <div class="room-information">
                  <div class="row">
                    <span class="first">Max:</span>
                    <span class="second"><img src="{{asset('template/images/ico/person.png')}}" alt="" /><img src="{{asset('template/images/ico/person.png')}}" alt="" /><img src="{{asset('template/images/ico/person.png')}}" alt="" /></span>
                  </div>
                  <div class="row">
                    <span class="first">Price:</span>
                    <span class="second">$ 55</span>
                  </div>
                  <div class="row">
                    <span class="first">Rooms:</span>
                    <span class="second">01</span>
                  </div>
                  <a href="booking-step1.html" class="gradient-button" title="Book">Book</a>
                </div>
                <div class="more-information">
                  <p>Stylish and individually designed room featuring a satellite TV, mini bar and a 24-hour room service menu.</p>
                  <p><strong>Room Facilities:</strong> Safety Deposit Box, Air Conditioning, Desk, Ironing Facilities, Seating Area, Heating, Shower, Bath, Hairdryer, Toilet, Bathroom, Pay-per-view Channels, TV, Telephone</p>
                  <p><strong>Bed Size(s):</strong> 1 Double </p>
                  <p><strong>Room Size:</strong>  16 square metres</p>
                </div>
              </li>
              <!--//room-->

              <!--room-->
              <li>
                <figure class="left"><img src="{{asset('template/images/slider/img.jpg')}}" alt="" width="270" height="152" /><a href="images/slider/img.jpg')}}" class="image-overlay" rel="prettyPhoto[gallery1]"></a></figure>
                <div class="meta">
                  <h2>Superior Double Room</h2>
                  <p>Prices are per room<br />20 % VAT Included in price</p>
                  <p>Non-refundable<br />Full English breakfast $ 24.80 </p>
                  <a href="javascript:void(0)" title="more info" class="more-info">+ more info</a>
                </div>
                <div class="room-information">
                  <div class="row">
                    <span class="first">Max:</span>
                    <span class="second"><img src="{{asset('template/images/ico/person.png')}}" alt="" /><img src="{{asset('template/images/ico/person.png')}}" alt="" /></span>
                  </div>
                  <div class="row">
                    <span class="first">Price:</span>
                    <span class="second">$ 55</span>
                  </div>
                  <div class="row">
                    <span class="first">Rooms:</span>
                    <span class="second">01</span>
                  </div>
                  <a href="booking-step1.html" class="gradient-button" title="Book">Book</a>
                </div>
                <div class="more-information">
                  <p>Stylish and individually designed room featuring a satellite TV, mini bar and a 24-hour room service menu.</p>
                  <p><strong>Room Facilities:</strong> Safety Deposit Box, Air Conditioning, Desk, Ironing Facilities, Seating Area, Heating, Shower, Bath, Hairdryer, Toilet, Bathroom, Pay-per-view Channels, TV, Telephone</p>
                  <p><strong>Bed Size(s):</strong> 1 Double </p>
                  <p><strong>Room Size:</strong>  16 square metres</p>
                </div>
              </li>
              <!--//room-->
            </ul>
          </article>
        </section>
        <!--//availability-->--}}

        <!--description-->
        <section id="description" class="tab-content">
          <article>
            <h1>Description</h1>
            <div class="text-wrap">
              <p>{!!$pakets->description!!}</p>
            </div>
          </article>
        </section>
        <!--//description-->

        <!--facilities-->
        <section id="facilities" class="tab-content">
          <article>
            <h1>Facilities</h1>
            <div class="text-wrap">
              <ul class="three-col">
                @foreach($facilities as $key => $faciliti)
                <li>{{$faciliti->name}}</li>
                @endforeach
              </ul>
            </div>
          </article>
        </section>
        <!--//facilities-->

        <!--location-->
        <section id="location" class="tab-content">
          <article>
            <!--map-->
            <iframe src="{{$pakets->lokasi}}" width="100%" height="500px" frameborder="0" style="border:0" allowfullscreen></iframe>
            <!--//map-->
          </article>
        </section>
        <!--//location-->

        <!--reviews-->
        {{--<section id="reviews" class="tab-content">
          <article>
            <h1>Hotel Score and Score Breakdown</h1>
            <div class="score">
              <span class="achieved">8 </span>
              <span> / 10</span>
              <p class="info">Based on 782 reviews</p>
              <p class="disclaimer">Guest reviews are written by our customers <strong>after their stay</strong> at Hotel Best Ipsum.</p>
            </div>

            <dl class="chart">
              <dt>Clean</dt>
              <dd><span id="data-one" style="width:80%;">8&nbsp;&nbsp;&nbsp;</span></dd>
              <dt>Comfort</dt>
              <dd><span id="data-two" style="width:60%;">6&nbsp;&nbsp;&nbsp;</span></dd>
              <dt>Location</dt>
              <dd><span id="data-three" style="width:80%;">8&nbsp;&nbsp;&nbsp;</span></dd>
              <dt>Staff</dt>
              <dd><span id="data-four" style="width:100%;">10&nbsp;&nbsp;&nbsp;</span></dd>
              <dt>Services</dt>
              <dd><span id="data-five" style="width:70%;">7&nbsp;&nbsp;&nbsp;</span></dd>
              <dt>Value for money</dt>
              <dd><span id="data-six" style="width:90%;">9&nbsp;&nbsp;&nbsp;</span></dd>
            </dl>
          </article>

          <article>
            <h1>Guest reviews</h1>
            <ul class="reviews">
              <!--review-->
              <li>
                <figure class="left"><img src="{{asset('template/images/uploads/avatar.jpg')}}" alt="avatar" /></figure>
                <address><span>Anonymous</span><br />Solo Traveller<br />Norway<br />22/06/2012</address>
                <div class="pro"><p>It was a warm friendly hotel. Very easy access to shops and underground stations. Staff very welcoming.</p></div>
                <div class="con"><p>noisy neigbourghs spoilt the rather calm environment</p></div>
              </li>
              <!--//review-->

              <!--review-->
              <li>
                <figure class="left"><img src="{{asset('template/images/uploads/avatar.jpg')}}" alt="avatar" /></figure>
                <address><span>Anonymous</span><br />Solo Traveller<br />Norway<br />22/06/2012</address>
                <div class="pro"><p>It was a warm friendly hotel. Very easy access to shops and underground stations. Staff very welcoming.</p></div>
                <div class="con"><p>noisy neigbourghs spoilt the rather calm environment</p></div>
              </li>
              <!--//review-->

              <!--review-->
              <li>
                <figure class="left"><img src="{{asset('template/images/uploads/avatar.jpg')}}" alt="avatar" /></figure>
                <address><span>Anonymous</span><br />Solo Traveller<br />Norway<br />22/06/2012</address>
                <div class="pro"><p>It was a warm friendly hotel. Very easy access to shops and underground stations. Staff very welcoming.</p></div>
                <div class="con"><p>noisy neigbourghs spoilt the rather calm environment</p></div>
              </li>
              <!--//review-->

              <!--review-->
              <li>
                <figure class="left"><img src="{{asset('template/images/uploads/avatar.jpg')}}" alt="avatar" /></figure>
                <address><span>Anonymous</span><br />Solo Traveller<br />Norway<br />22/06/2012</address>
                <div class="pro"><p>It was a warm friendly hotel. Very easy access to shops and underground stations. Staff very welcoming.</p></div>
                <div class="con"><p>noisy neigbourghs spoilt the rather calm environment</p></div>
              </li>
              <!--//review-->

              <!--review-->
              <li>
                <figure class="left"><img src="{{asset('template/images/uploads/avatar.jpg')}}" alt="avatar" /></figure>
                <address><span>Anonymous</span><br />Solo Traveller<br />Norway<br />22/06/2012</address>
                <div class="pro"><p>It was a warm friendly hotel. Very easy access to shops and underground stations. Staff very welcoming.</p></div>
                <div class="con"><p>noisy neigbourghs spoilt the rather calm environment</p></div>
              </li>
              <!--//review-->
            </ul>
          </article>
        </section>
        <!--//reviews-->--}}

        <!--things to do-->
        {{--<section id="things-to-do" class="tab-content">
          <article>
            <h1>London</h1>
            <figure class="left_pic"><img src="{{asset('template/images/uploads/img.jpg')}}" alt="Things to do - London general" /></figure>
            <p class="teaser">London is a diverse and exciting city with some of the best sights and attractions in the world. </p>
            <p>See London from above on the London Eye; meet a celebrity at Madame Tussauds; examine some of the world’s most precious treasures at the British Museum or come face-to-face with the dinosaurs at the Natural History Museum.</p>

            <h1>Sports and nature</h1>
            <figure class="left_pic"><img src="{{asset('template/images/uploads/img.jpg')}}" alt="Things to do - London Sports and nature" /></figure>
            <p class="teaser">London is one of the greenest capitals in the world, with plenty of green and open spaces. There are more than 3000 open spaces.</p>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.<br />Ut wisi enim ad minim veniam, quis nostrud exerci. </p>

            <h1>Nightlife</h1>
            <figure class="left_pic"><img src="{{asset('template/images/uploads/img.jpg')}}" alt="Things to do - London Nightlife" /></figure>
            <p class="teaser">Looking for nightclubs in London? Take a look at our guide to London clubs. Browse for club ideas, regular club nights and one-off events. </p>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.<br />Ut wisi enim ad minim veniam, quis nostrud exerci. </p>

            <h1>Culture and history</h1>
            <figure class="left_pic"><img src="{{asset('template/images/uploads/img.jpg')}}" alt="Things to do - London general" /></figure>
            <p class="teaser">For a display of British pomp and ceremony, watch the Changing the Guard ceremony outside Buckingham Palace.</p>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.<br />Ut wisi enim ad minim veniam, quis nostrud exerci. </p>
            <hr />
            <a href="#" class="gradient-button right" title="Read more">Read more</a>
          </article>
        </section>--}}
        <!--//things to do-->
      </section>
      <aside class="right-sidebar">
        <article class="testimonials clearfix center">
          <!-- <blockquote>Loved the staff and the location was just amazing... Perfect!” </blockquote>
          <span class="name">- Jane Doe, Solo Traveller</span> -->
        <center>  <a href="#editpax" class="gradient-button edit">Book Now</a>
          <!-- <button type="button" class="gradient-button" name="button">Book Now</button> -->
          <div class="edit_field" id="editpax">
            <h2>Pilih jumlah pax</h2>
            <form class="" action="{{route('form-pesenger')}}" method="post">
              @csrf
              <input type="hidden" name="action" value="editpax">
              <input type="hidden" name="ids" value="{{$pakets->id}}">
              <div  style="max-width:50px;">
                <select class="selector" name="jumlah_pax" required>
                  <?php for ($i=$pakets->min_pax; $i < $pakets->max_pax+1; $i++) {
                    echo "<option value=".$i.">".$i."</option>";
                  } ?>
                </select>
              </div>
              <br>
              <input type="submit" value="Lanjutkan" class="gradient-button" id="submit1"/>
              <a href="#editpax" class="gradient-button" style="background:red;"> Cancel</a>
            </form>
          </div>
        </center>
        </article>
        <!--Need Help Booking?-->
        <article class="default clearfix">
          <h2>Need Help Booking?</h2>
          <p>Call our customer services team on the number below to speak to one of our advisors who will help you with all of your holiday needs.</p>
          <?php $webs = App\Web::find(1); ?>
          <p class="phone-green">{{$webs->telp}}</p>
          <p class="phone-green">{{$webs->hp}}</p>
          <p class="email-green"><a href="#">{{$webs->email}}</a></p>
        </article>
        <!--//Need Help Booking?-->

        <!--Why Book with us?-->
        <article class="default clearfix">
          <h2>Why Book with us?</h2>
          <h3>Low rates</h3>
          <p>Get the best rates, or get a refund.<br />No booking fees. Save money!</p>
          <h3>Largest Selection</h3>
          <p>140,000+ hotels worldwide<br />130+ airlines<br />Over 3 million guest reviews</p>
          <h3>We’re Always Here</h3>
          <p>Call or email us, anytime<br />Get 24-hour support before, during, and after your trip</p>
        </article>
        <!--//Why Book with us?-->
      </aside>

    </div>
  </div>
</div>
<script type="text/javascript">
     $(document).ready(function() {
          $('#image-gallery').lightSlider({
              gallery:true,
              item:1,
              thumbItem:6,
              slideMargin: 0,
              speed:500,
              auto:true,
              loop:true,
              onSliderLoad: function() {
                  $('#image-gallery').removeClass('cS-hidden');
              }
          });

    // $('#gallery1,#gallery2,#gallery3,#gallery4').lightGallery({
    //   download:false
    // });
  });
  </script>
@endsection
