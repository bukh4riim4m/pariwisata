@extends('layouts.umum_app')

@section('content')
<div class="main" role="main">
  <div class="wrap clearfix">
    <!--main content-->
    <div class="content clearfix">
      <!--breadcrumbs-->
      <nav role="navigation" class="breadcrumbs clearfix">
        <!--crumbs-->
        <ul class="crumbs">
          <li><a href="{{route('index')}}" title="Dashboard">Dashboard</a></li>
          <li>Blog</li>
        </ul>
        <!--//crumbs-->
      </nav>
      <div class="row">
				<!--three-fourth content-->
				<div class="three-fourth">
					<article class="static-content post" style="background-color:#ffffff;margin-bottom:15px;padding:10px;">
							<h2><span>{{$blogs->title}}</span></h2>
							<p class="entry-meta">
								<span class="date">Date: {{date('d M Y H:i', strtotime($blogs->updated_at))}}</span>
							</p>
						<div class="entry-content">
							<p>{!!$blogs->description!!}</p>
						</div>
					</article>
          <!--three-fourth content-->
  					<!-- <section class="three-fourth"> -->
  						<form id="booking" method="post" action="{{route('komentar',$blogs->id)}}" class="booking">
                @csrf
  							<fieldset>
  								<h2><span>KOMENTAR </span></h2>
  								<div class="row">
  									<div class="f-item">
  										<label for="email">Email address</label>
                    </div>
  										<input type="email" id="email" name="email" />

  									<!-- <div class="f-item">
  										<label for="confirm_email">Confirm email address</label>
  										<input type="text" id="confirm_email" name="confirm_email" />
  									</div> -->
  								</div>

  								<div class="row">
  									<div class="f-item">
  										<label>Special requirements: <span>(Not Guaranteed)</span></label>
                      </div>
  										<textarea rows="10" cols="10"></textarea>

  								</div>
  								<input type="submit" class="gradient-button" value="Send" id="next-step" />
  							</fieldset>
  						</form>
  					<!-- </section> -->
  				<!--//three-fourth content-->
					<!--//bottom navigation-->
				</div>
				<!--//three-fourth$blog content-->

        <!--sidebar-->
        @include('includes.right_sidebar')
				<!--//sidebar-->
			</div>
    </div>
  </div>
</div>
@endsection
