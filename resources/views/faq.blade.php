@extends('layouts.umum_app')

@section('content')
<div class="main" role="main">
  <div class="wrap clearfix">
    <!--main content-->
    <div class="content clearfix">
      <!--breadcrumbs-->
      <nav role="navigation" class="breadcrumbs clearfix">
        <!--crumbs-->
        <ul class="crumbs">
          <li><a href="{{route('index')}}" title="Dashboard">Dashboard</a></li>
          <li>Faq</li>
        </ul>
        <!--//crumbs-->
      </nav>
      <div class="row">
				<!--three-fourth content-->
				<div class="three-fourth">
					<!--post-->
          @foreach($blogs as $key => $blog)
					<article class="static-content post" style="background-color:#ffffff;margin-bottom:15px;padding:10px;">
						<div class="entry-content">
							<p>{!!$blog->description!!}</p>
						</div>
					</article>
          @endforeach
					<!--//post-->
          @if(count($blogs) < 1)
          <article class="static-content post" style="background-color:#ffffff;margin-bottom:15px;padding:10px;">
						<div class="entry-content">
							<p style="text-align:center;">Belum ada postingan</p>
						</div>
					</article>
          @endif
					<!--//bottom navigation-->
				</div>
				<!--//three-fourth content-->

				<!--sidebar-->
        @include('includes.right_sidebar')
				<!--//sidebar-->
			</div>
    </div>
  </div>
</div>
@endsection
