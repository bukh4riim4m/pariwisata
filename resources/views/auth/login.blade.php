
@extends('layouts.umum_app')

@section('content')
  <div class="lightbox" style="display:block;">
		<div class="lb-wrap">
			<a href="#" class="close">x</a>
			<div class="lb-content">
				<form action="{{route('login')}}" method="post">
          @csrf
					<h1>Log in</h1>
					<div class="f-item">
						<label for="email">E-mail address</label>
						<input type="email" id="email" name="email" />
					</div>
					<div class="f-item">
						<label for="password">Password</label>
						<input type="password" id="password" name="password" />
					</div>
					<div class="f-item checkbox">
						<input type="checkbox" id="remember_me" name="checkbox" />
						<label for="remember_me">Remember me next time</label>
					</div>
					<p><a href="#" title="Forgot password?">Forgot password?</a><br><br>
					<!-- Dont have an account yet? <a href="register.html" title="Sign up">Sign up.</a></p> -->
					<input type="submit" id="login" name="login" value="login" class="gradient-button"/>
				</form>
			</div>
		</div>
	</div>
	@endsection
