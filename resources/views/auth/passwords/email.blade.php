@extends('layouts.umum_app')

@section('content')
<div class="main" role="main">
  <div class="wrap clearfix">
    <!--main content-->
    <div class="content clearfix">
      <!--breadcrumbs-->
      <nav role="navigation" class="breadcrumbs clearfix">
        <!--crumbs-->
        <ul class="crumbs">
          <li><a href="{{route('index')}}" title="Dashboard">Dashboard</a></li>
          <li>Reset Password</li>
        </ul>
        <!--//crumbs-->
      </nav>


      <div class="row">
        <!--three-fourth content-->
        <div class="three-fourth">
          <article class="static-content post" style="background-color:#ffffff;margin-bottom:15px;padding:10px;">

                <h2><span>{{ __('Reset Password') }}</span></h2>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">
                        @csrf

                        <div class="row">
        									<div class="f-item">
                            <label for="email">{{ __('E-Mail Address') }}</label>
                          </div>
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <br><br>
                        <button type="submit" class="gradient-button">
                            {{ __('Send Password Reset Link') }}
                        </button>
                        </div>
                    </form>
                </div>
                </div>
              </article>
              @include('includes.right_sidebar')
            </div>
        </div>
    </div>
</div>
</div>
@endsection
