@extends('layouts.umum_app')

@section('content')
<div class="main" role="main">
  <div class="wrap clearfix">
    <!--main content-->
    <div class="content clearfix">
      <!--breadcrumbs-->
      <nav role="navigation" class="breadcrumbs clearfix">
        <!--crumbs-->
        <ul class="crumbs">
          <li><a href="{{route('index')}}" title="Dashboard">Dashboard</a></li>
          <li>Blog</li>
        </ul>
        <!--//crumbs-->
      </nav>
      <div class="row">
				<!--three-fourth content-->
				<div class="three-fourth">
					<!--post-->
          @foreach($blogs as $key => $blog)
					<article class="static-content post" style="background-color:#ffffff;margin-bottom:15px;padding:10px;">
							<h2><a href="">{{$blog->title}}</a></h2>
							<p class="entry-meta">
								<span class="date">Date: {{date('d M Y H:i', strtotime($blog->updated_at))}}</span>
								<!-- <span class="author">By: admin</span>
								<span class="tags">Tags:
									<a rel="category tag" title="View all posts in Travel" href="#">Travel</a>,
									<a rel="category tag" title="View all posts in Photography" href="#">Photography</a>
								</span>
								<span class="comments"><a href="#">4 Comments</a></span> -->
							</p>
              <div class="entry-content">
              <p>  {!!$blog->description!!}</p>
                <a href="{{route('blogdetail',$blog->id)}}" class="gradient-button">Continue reading</a>
              </div>
					</article>

          @endforeach
					<!--//post-->
          @if(count($blogs) < 1)
          <article class="static-content post" style="background-color:#ffffff;margin-bottom:15px;padding:10px;">
						<div class="entry-content">
							<p style="text-align:center;">Belum ada postingan</p>
						</div>
					</article>
          @endif
          {{$blogs->render()}}
					<!--//bottom navigation-->
				</div>
				<!--//three-fourth content-->

        <!--sidebar-->
        @include('includes.right_sidebar')
				<!--//sidebar-->
			</div>
    </div>
  </div>
</div>
@endsection
