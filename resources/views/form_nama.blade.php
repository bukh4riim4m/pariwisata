@extends('layouts.umum_app')

@section('content')
<div class="main" role="main">
  <div class="wrap clearfix">
    <!--main content-->
    <div class="content clearfix">
      <!--breadcrumbs-->
      <nav role="navigation" class="breadcrumbs clearfix">
        <!--crumbs-->
        <ul class="crumbs">
          <li><a href="{{route('index')}}" title="Dashboard">Dashboard</a></li>
          <li><a href="{{route('tours')}}" title="Paket Tour">Tours</a></li>
          <li><a href="#" title="Form Add">Form Pessenger</a></li>
        </ul>
        <ul class="top-right-nav">
					<!-- <li><a href="#" title="Back to results"><button  type="button" class="gradient-button" name="button">TAMBAH</button></a></li> -->
					<li><a href="{{route('tours')}}" title="Back">Back</a></li>
				</ul>
        <!--//crumbs-->

      </nav>
      <!--three-fourth content-->
        <section class="three-fourth">
          <form id="booking" method="post" action="{{route('informasi-transfer')}}" class="booking">
            @csrf
            <input type="hidden" name="action" value="add" required>
            <input type="hidden" name="ids" value="{{$ids}}" required>
            <input type="hidden" name="jumlah_pax" value="{{$forms}}">
            <fieldset>
              <h3><span>DATA </span>PESSENGER</h3>
              <?php for ($i=1; $i < $forms+1; $i++) {
                echo "<div class='row twins'>
                  <div class='f-item'>
                    <label for='card_number'>".$i.". Nama Lengkap</label>
                    <input type='text' name='name[]' placeholder='...' required/>
                  </div>
                  <div class='f-item'>
                    <label for='card_number'>No telp</label>
                    <input type='text' name='hp[]' placeholder='...' required/>
                  </div>
                  <span class='info'>diisi dengan data yang berangkat</span>
                </div>";
              } ?>
              <hr />
              <h2>
                <span>DATA </span>PEMESAN</h2>
              <div class="row triplets">
                <div class="f-item">
                  <label for="card_number">Nama Lengkap</label>
                  <input type="text" name="nama_pemesan" placeholder="..." required/>
                </div>
                <div class="f-item">
                  <label for="card_number">No telp</label>
                  <input type="text" name="telp" placeholder="..." required/>
                </div><span class="info">diisi dengan data pemesan</span>
              </div>
              <div class="row">
                <div class="f-item">
                  <label for="card_number">Email</label>
                  <input type="email" name="email" placeholder="..." required/>
                </div>

              </div>
              <hr />
              <h2>REKENING KAMI</h2>
              <div class="row triplets">
                <div class="f-item">
                  <label for="card_number">Rekening Tujuan</label>
                  <?php $banks = App\Bank::get(); ?>
                  <select class="" name="rekening" required>
                    @foreach($banks as $bank)
                    <option value="{{$bank->id}}">{{$bank->nama_bank}}</option>
                    @endforeach
                  </select>
                </div>
                <span class="info">Pilih rekening tujuan transfer</span>

              </div>
              <hr />
              <input type="submit" class="gradient-button" value="Lanjutkan" id="next-step" />
              <a href="#" class="gradient-button" style="background:red;">Batal</a>
            </fieldset>
          </form>
        </section>
      <!--//three-fourth content-->

      <!--right sidebar-->
      @include('includes.right_sidebar')
      <!--//right sidebar-->
    </div>
  </div>
</div>
@endsection
