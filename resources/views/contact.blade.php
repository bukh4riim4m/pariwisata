@extends('layouts.umum_app')

@section('content')
<?php $contacts = App\Web::find(1); ?>
<div class="main" role="main">
  <div class="wrap clearfix">
    <!--main content-->
    <div class="content clearfix">
      <!--breadcrumbs-->
      <nav role="navigation" class="breadcrumbs clearfix">
        <!--crumbs-->
        <ul class="crumbs">
          <li><a href="{{route('index')}}" title="Dashboard">Dashboard</a></li>
          <li>Contact Us</li>
        </ul>
        <!--//crumbs-->
      </nav>
      <!--//breadcrumbs-->

      <!--three-fourth content-->
      <section class="three-fourth">
        <!-- <h1>Contact us</h1> -->
        <!--map-->
        <div class="map-wrap">
          <iframe src="{{$contacts->maps}}" width="100%" height="500px" frameborder="0" style="border:0" allowfullscreen></iframe>
          <!-- <div class="gmap" id="map_canvas"></div> -->
        </div>
        <!--//map-->
      </section>
      <!--three-fourth content-->

      <!--sidebar-->
      <aside class="right-sidebar">
        <!--contact form-->
        <article class="default">
          <h2>Send us a message</h2>
          <form action="" id="contact-form" method="post">
            <fieldset>
              <div class="f-item">
                <label for="name">Your name</label>
                <input type="text" id="name" name="name" value="" required="required" />
              </div>
              <div class="f-item">
                <label for="email">Your e-mail</label>
                <input type="email" id="email" name="email" value="" required="required"  />
              </div>
              <div class="f-item">
                <label for="message">Your message</label>
                <textarea id="message" rows="10" cols="10"></textarea>
              </div>
              <input type="submit" value="Send" id="submit" name="submit" class="gradient-button" />
            </fieldset>
          </form>
        </article>
        <!--//contact form-->

        <!--contact info-->
        <?php $webs = App\Web::find(1); ?>
        <article class="default">
          <h2>Or contact us directly</h2>
          <p class="phone-green">{{$webs->telp}}</p>
          <p class="phone-green">{{$webs->hp}}</p>
          <p class="email-green"><a href="#">{{$webs->email}}</a></p>
        </article>
        <!--//contact info-->
      </aside>
      <!--//sidebar-->
    </div>
    <!--//main content-->
  </div>
</div>
<!--//main-->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
function initialize() {
  var secheltLoc = new google.maps.LatLng(<?php echo $webs->maps; ?>);

  var myMapOptions = {
     zoom: 15
    ,center: secheltLoc
    ,mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var theMap = new google.maps.Map(document.getElementById("map_canvas"), myMapOptions);


  var marker = new google.maps.Marker({
    map: theMap,
    draggable: true,
    position: new google.maps.LatLng(<?php echo $webs->maps; ?>),
    visible: true
  });

  var boxText = document.createElement("div");
  boxText.innerHTML = "<strong>{{$webs->title_web}}</strong><br>NTB,<br>Indonesia";

  var myOptions = {
     content: boxText
    ,disableAutoPan: false
    ,maxWidth: 0
    ,pixelOffset: new google.maps.Size(-140, 0)
    ,zIndex: null
    ,closeBoxURL: ""
    ,infoBoxClearance: new google.maps.Size(1, 1)
    ,isHidden: false
    ,pane: "floatPane"
    ,enableEventPropagation: false
  };

  google.maps.event.addListener(marker, "click", function (e) {
    ib.open(theMap, this);
  });

  var ib = new InfoBox(myOptions);
  ib.open(theMap, marker);
}
</script>
@endsection
