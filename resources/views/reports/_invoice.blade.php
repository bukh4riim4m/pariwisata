
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel='dns-prefetch' href='//maps.google.com' />
    <link rel="stylesheet" href="{{asset('template/css/style.css')}}" type="text/css" media="screen,projection,print" />
    <link rel="stylesheet" href="{{asset('template/css/prettyPhoto.css')}}" type="text/css" media="screen" />
    <link rel="shortcut icon" href="{{asset('template/images/txt/doang.png')}}" />
    <!-- TAMBAH BARU DARI WEBSITE ASLINYA -->
      <link rel="stylesheet" href="{{asset('template/css/styler.css')}}"/>
      <link rel="stylesheet" href="{{asset('template/css/lightslider.min.css')}}" />
    <style>
    #customers {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        font-size: 13px;
        width: 100%;
    }

    #customers td, #customers th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #customers tr:nth-child(even){background-color: #f2f2f2;}

    /* #customers tr:hover {background-color: #ddd;} */

    #customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #41AFAA;
        color: white;
    }
    </style>
    <title></title>
  </head>
  <body>

            <section class="three-fourth">

              <div class="booking">
                <fieldset>
                <center><h2 style="font-size:20px;">INVOICE PEMBAYARAN</h2></center><br><br>
                <table width="100%">
                  <tr>
                    <td colspan="2"><h3>PACKAGE : <span>{{$pemesanans->paketId->nama_paket}} </span></h3></td>
                  </tr>
                  <tr>
                    <td>
                        <figure><a href="" title=""><img src="{{asset('template/images/uploads/'.$pemesanans->paketId->gambar->gambar)}}" alt="" width="270" height="152" /></a></figure>
                    </td>
                    <td><br><br>
                        <table id="customers">
                          <tr>
                            <td colspan="2"><h5>{{$pemesanans->paketId->day}}</h5> </td>
                          </tr>
                          <tr>
                            <td>Nama Pemesan </td><td> : {{$pemesanans->nama_pemesan}}</td>
                          </tr>
                          <tr>
                            <td>Jumlah Pax </td><td> : {{$pemesanans->jumlah_pax}}</td>
                          </tr>
                          <tr>
                            <td>Harga / Pax </td><td> : IDR {{number_format($pemesanans->total_harga / $pemesanans->jumlah_pax,0,",",".")}}</td>
                          </tr>
                        </table>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2"><hr></td>
                  </tr>
                  <tr>
                    <td colspan="2">
                      <h2><span>DATA </span>PAX</h2>
                      <table class="table" id="customers">
                        <tr>
                          <th style="max-width:2px;">No.</th>
                          <th>Nama</th>
                          <th>No. Telp</th>
                        </tr>
                        @foreach($pessengers as $key => $pessenger)
                        <tr>
                          <td>{{$key+1}}.</td><td>{{$pessenger->nama}}</td><td>{{$pessenger->hp}}</td>
                        </tr>
                        @endforeach
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2"><hr></td>
                  </tr>
                  <tr>
                    <td colspan="2"><h2>INFO TRANSFER</h2></td>
                  </tr>
                  <tr>
                    <td>
                      <label for="card_number">Rekening Tujuan</label> <br><br>
                      <img src="{{asset('template/images/txt/'.$pemesanans->bankId->logo)}}" alt="" width="100px;">
            <br><br>
                      <p style="font-size:14px;">{{$pemesanans->bankId->nama_bank}}<br>
                        No Rek : {{$pemesanans->bankId->rekening}}<br>
                        A/N : {{$pemesanans->bankId->atas_nama}}
                      </p>
                    </td>
                    <td><br><br>
                      <table id="customers">
                        <tr>
                          <td>#INVOICE </td>
                          <td>: {{$pemesanans->invoice}}</td>
                        </tr>
                        <tr>
                          <td>TOTAL HARGA </td>
                          <td>: IDR {{number_format($pemesanans->total_harga,0,",",".")}}</td>
                        </tr>
                        <tr>
                          <td>KODE UNIK </td>
                          <td>: {{$pemesanans->kode_unik}}</td>
                        </tr>
                        <tr>
                          <td><em>TOTAL TRANSFER</em> </td>
                          <td>:<em> IDR {{number_format($pemesanans->total_harga+$pemesanans->kode_unik,0,",",".")}}</em></td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>


                </fieldset>
              </div>
            </section>

  </body>
</html>
