<!--footer-->
<?php $webs = App\Web::find(1); ?>
<footer>
  <div class="wrap clearfix">
    <!--column-->
    <article class="one-fourth">
      <h3>{{$webs->title_web}}</h3>
      <p>{{$webs->alamat}}</p>
      <p><em>P:</em> 24/7 customer support: <br> {{$webs->telp}}<br>{{$webs->hp}}</p>
      <p><em>E:</em> <a href="#" title="{{$webs->email}}">{{$webs->email}}</a></p>
    </article>
    <!--//column-->

    <!--column-->
    <article class="one-fourth">
      <h3>Customer support</h3>
      <ul>
        <li><a href="{{route('faq')}}" title="Faq">Faq</a></li>
        <li><a href="{{route('reservation')}}" title="How do I make a reservation?">How do I make a reservation?</a></li>
        <li><a href="{{route('payment-option')}}" title="Payment options">Payment options</a></li>
        <li><a href="{{route('booking-tips')}}" title="Booking tips">Booking tips</a></li>
      </ul>
    </article>
    <!--//column-->

    <!--column-->
    <article class="one-fourth">
      <h3>Follow us</h3>
      <ul class="social">
        <li class="facebook"><a href="#" title="facebook">facebook</a></li>
        <li class="youtube"><a href="#" title="youtube">youtube</a></li>
        <!-- <li class="rss"><a href="#" title="rss">rss</a></li> -->
        <li class="linkedin"><a href="#" title="linkedin">linkedin</a></li>
        <li class="googleplus"><a href="#" title="googleplus">googleplus</a></li>
        <li class="twitter"><a href="#" title="twitter">twitter</a></li>
        <!-- <li class="vimeo"><a href="#" title="vimeo">vimeo</a></li>
        <li class="pinterest"><a href="#" title="pinterest">pinterest</a></li> -->
      </ul>
    </article>
    <!--//column-->

    <!--column-->
    <article class="one-fourth last">
      <h3>Don’t miss our exclusive offers</h3>
      <form id="newsletter" action="newsletter.php" method="post">
        <fieldset>
          <input type="email" id="newsletter_signup" name="newsletter_signup" placeholder="Enter your email here" />
          <input type="submit" id="newsletter_submit" name="newsletter_submit" value="Signup" class="gradient-button" />
        </fieldset>
      </form>
    </article>
    <!--//column-->

    <section class="bottom">

      <p class="copy">Copyright 2018 <em>{{$webs->title_web}}</em> By <em>bukhariimam44@gmail.com</em></p>
      {{--<nav>
        <ul>
          <li><a href="#" title="About us">About us</a></li>
          <li><a href="contact.html" title="Contact">Contact</a></li>
          <li><a href="#" title="Partners">Partners</a></li>
          <li><a href="#" title="Customer service">Customer service</a></li>
          <li><a href="#" title="FAQ">FAQ</a></li>
          <li><a href="#" title="Careers">Careers</a></li>
          <li><a href="#" title="Terms & Conditions">Terms &amp; Conditions</a></li>
          <li><a href="#" title="Privacy statement">Privacy statement</a></li>
        </ul>
      </nav>--}}
    </section>
  </div>
</footer>
<!--//main-->
<div class="lightbox" style="display:none;" id="login_lightbox">
  <div class="lb-wrap">
    <a href="javascript:void(0);" class="close" style="background-color:red;">x</a>
    <div class="lb-content">
      <form action="{{route('login')}}" method="post">
        @csrf
        <h1>Log in</h1>
        <div class="f-item">
          <label for="email">E-mail address</label>
          <input type="email" id="email" name="email" required/>
        </div>
        <div class="f-item">
          <label for="password">Password</label>
          <input type="password" id="password" name="password" required/>
        </div>
        <div class="f-item checkbox">
          <input type="checkbox" id="remember_me" name="checkbox" />
          <label for="remember_me">Remember me next time</label>
        </div>
        <p><a href="{{route('password.request')}}" title="Forgot password?">Forgot password?</a><br><br>
        <!-- Dont have an account yet? <a href="register.html" title="Sign up">Sign up.</a></p> -->
        <input type="submit" id="login" name="login" value="login" class="gradient-button"/>
      </form>
    </div>
  </div>
</div>
