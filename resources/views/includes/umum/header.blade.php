<header>
  <div class="wrap clearfix">
    <!--logo-->
    <?php $webs = App\Web::find(1); ?>
    <h1 class="logo"><a href="{{route('index')}}" title="Book Your Travel - home"><img src="{{asset('template/images/txt/'.$webs->logo)}}" /></a></h1>
    <!--//logo-->

    <!--ribbon-->
    @include('includes.umum.ribbon')
    <!--//ribbon-->

    <!--search-->
    <div class="search">
      <form id="search-form" method="get" action="search-form">
        <input type="search" placeholder="Search entire site here" name="site_search" id="site_search" />
        <input type="submit" id="submit-site-search" value="submit-site-search" name="submit-site-search"/>
      </form>
    </div>
    <!--//search-->

    <!--contact
    <div class="contact">
      <span class="number"></span>
      <span class="number">{{$webs->hp}}</span>
    </div>-->
    <!--//contact-->
  </div>

  <!--main navigation-->
  <nav class="main-nav" role="navigation" id="nav" style="background:white;color:#666;">
    <ul class="wrap">
      <li class="{{setActive(['index'])}}"><a href="{{route('index')}}" title="Dashboard" style="color:#000;text-transform: capitalize;">Dashboard</a></li>
      <li class="{{setActive(['tours','detail-tour','form-pesenger'])}}"><a href="{{route('tours')}}" style="color:#000;text-transform: capitalize;" title="TOURS">Tours</a></li>
      <li class="{{setActive(['about'])}}"><a href="{{route('about')}}" style="color:#000;text-transform: capitalize;" title="ABOUT">About</a></li>
      <li class="{{setActive(['blog','blogdetail'])}}"><a href="{{route('blog')}}" style="color:#000;text-transform: capitalize;" title="BLOG">Blog</a></li>
      <li class="{{setActive(['gallery','gallery-detail'])}}"><a href="{{route('gallery')}}" style="color:#000;text-transform: capitalize;" title="GALERI">Gallery</a></li>
      <li class="{{setActive(['contact'])}}"><a href="{{route('contact')}}" style="color:#000;text-transform: capitalize;" title="CONTACT">Contact</a></li>
    </ul>
  </nav>
  <!--//main navigation-->
</header>
<!--//header-->
