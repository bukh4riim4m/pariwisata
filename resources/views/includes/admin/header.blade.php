<header>
  <div class="wrap clearfix">
    <!--logo-->
    <?php $webs = App\Web::find(1); ?>
    <h1 class="logo"><a href="{{route('index')}}" title="Book Your Travel - home"><img src="{{asset('template/images/txt/'.$webs->logo)}}" /></a></h1>
    <!--//logo-->

    <!--ribbon-->

    <!--//ribbon-->

    <!--search-->
    <div class="search">
      <form id="search-form" method="get" action="search-form">
        <input type="search" placeholder="Search entire site here" name="site_search" id="site_search" />
        <input type="submit" id="submit-site-search" value="submit-site-search" name="submit-site-search"/>
      </form>
    </div>
    <!--//search-->

    <!--contact-->
    <div class="contact">
      <span class="number"></span>
      <span class="number">{{$webs->hp}}</span>
    </div>
    <!--//contact-->
  </div>

  <!--main navigation-->
  <nav class="main-nav" role="navigation" id="nav">
    <ul class="wrap">
      <li class="{{setActive(['admin-home'])}}"><a href="{{route('admin-home')}}" title="Home">HOME</a></li>
      <li class="{{setActive(['admin-paket-tour','admin-detail-tour','admin-add-tour'])}}"><a href="{{route('admin-paket-tour')}}" title="Paket Tour">PAKET TOUR</a></li>
      <li class="{{setActive(['admin-gallery','admin-gallery-anak'])}}"><a href="{{route('admin-gallery')}}" title="Gallery">Gallery</a></li>
      <li class="{{setActive(['admin-settings','admin-settings-blog-edit'])}}"><a href="{{route('admin-settings')}}" title="Settings">SETTING</a></li>
      <li class="{{setActive(['admin-blog','admin-add-blog','admin-blog-edit'])}}"><a href="{{route('admin-blog')}}" title="Blog">BLOG</a></li>
      <li><a href="{{route('admin-logout')}}" title="Logout">LOGOUT</a></li>

    </ul>
  </nav>
  <!--//main navigation-->
</header>
<!--//header-->
