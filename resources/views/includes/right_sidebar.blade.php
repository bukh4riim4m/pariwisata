<aside class="right-sidebar">
  <article class="default clearfix">
    <h2>Need Help Booking?</h2>
    <p>Call our customer services team on the number below to speak to one of our advisors who will help you with all of your holiday needs.</p>
    <?php $webs = App\Web::find(1); ?>
    <p class="phone-green">{{$webs->telp}}</p>
    <p class="phone-green">{{$webs->hp}}</p>
    <p class="email-green"><a href="#">{{$webs->email}}</a></p>
  </article>
  <!--//Need Help Booking?-->

  <!--Why Book with us?-->
  <article class="default clearfix">
    <h2>Why Book with us?</h2>
    <h3>Low rates</h3>
    <p>Get the best rates, or get a refund.<br />No booking fees. Save money!</p>
    <h3>Largest Selection</h3>
    <p>140,000+ hotels worldwide<br />130+ airlines<br />Over 3 million guest reviews</p>
    <h3>We’re Always Here</h3>
    <p>Call or email us, anytime<br />Get 24-hour support before, during, and after your trip</p>
  </article>
  <!--//Why Book with us?-->

</aside>
