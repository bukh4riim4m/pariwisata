@extends('layouts.umum_app')

@section('content')
<div class="main" role="main">
  <div class="wrap clearfix">
    <!--main content-->
    <div class="content clearfix">
      <!--breadcrumbs-->
      <nav role="navigation" class="breadcrumbs clearfix">
        <!--crumbs-->
        <ul class="crumbs">
          <li><a href="{{route('index')}}" title="Dashboard">Dashboard</a></li>
          <li>Gallery</li>
        </ul>
        <!--//crumbs-->
      </nav>
      <!--//breadcrumbs-->

      <!--three-fourth content-->
      <section class="full">
        <div class="deals clearfix">
          @foreach($galleries as $key => $gallery)
          <article class="one-fourth">
  					<figure><a href="#" title=""><img src="{{asset('template/images/gallery/'.$gallery->img_gallery)}}" alt="" width="270" height="152" /></a></figure>
  					<div class="details">
  						<h4>{{$gallery->title}}</h4>
              <a href="{{route('gallery-detail',$gallery->id)}}" title="Detail" class="gradient-button">Detail</a>
            </div>

  				</article>
          @endforeach
          </div>
      </section>
      <!--three-fourth content-->
    </div>
    <!--//main content-->
  </div>
</div>
@endsection
