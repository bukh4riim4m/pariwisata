@extends('layouts.admin_app')

@section('content')
<div class="main" role="main">
  <div class="wrap clearfix">
    <!--main content-->
    <div class="content clearfix">
      <!--breadcrumbs-->
      <nav role="navigation" class="breadcrumbs clearfix">
        <!--crumbs-->
        <ul class="crumbs">
          <li><a href="{{route('home')}}" title="Home">Home</a></li>
          <li><a href="{{route('admin-gallery')}}" title="Gallery">Gallery</a></li>
          <li>{{$title->title}}</li>
        </ul>
        <ul class="top-right-nav">
          <li><a href="#add" class="gradient-button edit"> <button type="button" class="gradient-button" name="button">ADD DETAIL GALLERY</button> </a></li>
				</ul>
        <!--//crumbs-->
      </nav>
      <!--//breadcrumbs-->
      <div class="edit_field" id="add">
        <form class="" action="{{route('admin-gallery-anak',$id_induk)}}" method="post" enctype="multipart/form-data">
          @csrf
          <input type="hidden" name="action" value="add" required>
          <label for="new_name">Gambar:</label>
          <input type="file" name="gambar" required/>
          <input type="submit" value="Simpan" class="gradient-button" id="submit"/>
          <a href="#">Cancel</a>
        </form>
      </div>
      <!--three-fourth content-->
      <section class="full">
        <div class="deals clearfix">

          <article class="one-fourth">
  					<figure><a href="#" title=""><img src="{{asset('template/images/gallery/'.$title->img_gallery)}}" alt="" width="270" height="152" /></a></figure>
  					<!-- <div class="details">
  						<h4>{{$title->title}} 1</h4>
  					</div> -->
  				</article>
          @foreach($galleries as $key => $gallery)
          <article class="one-fourth">
  					<figure><a href="#" title=""><img src="{{asset('template/images/gallery/'.$gallery->img_gallery)}}" alt="" width="270" height="152" /></a></figure>
  					<!-- <div class="details">
  						<h4>{{$title->title}} {{$key+2}}</h4>
  					</div> -->
  				</article>
          @endforeach
          <!--//item-->
</div>
        <!--//get inspired list-->
      </section>

      <!--three-fourth content-->
    </div>
    <!--//main content-->
  </div>
</div>
@endsection
