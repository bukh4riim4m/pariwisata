@extends('layouts.admin_app')

@section('content')
<div class="main" role="main">
  <div class="wrap clearfix">
    <!--main content-->
    <div class="content clearfix">
      <!--breadcrumbs-->
      <nav role="navigation" class="breadcrumbs clearfix">
        <!--crumbs-->
        <ul class="crumbs">
          <li><a href="{{route('home')}}" title="Home">Home</a></li>
          <li>Gallery</li>
        </ul>
        <ul class="top-right-nav">
          <li><a href="#add" class="gradient-button edit"> <button type="button" class="gradient-button" name="button">ADD GALLERY</button> </a></li>

					<!-- <li><a href="#" title="Change search">Change search</a></li> -->
				</ul>
        <!--//crumbs-->
      </nav>
      <!--//breadcrumbs-->

      <!--three-fourth content-->
      <section class="full">
        <div class="deals clearfix">
          <!--item-->
          <div class="edit_field" id="add">
            <form class="" action="{{route('admin-gallery')}}" method="post" enctype="multipart/form-data">
              @csrf
              <input type="hidden" name="action" value="addinduk" required>
              <label for="new_name">Title Gallery:</label>
              <input type="text" name="title" value=""/ required>
              <label for="new_name">Gambar:</label>
              <input type="file" name="gambar" required/>
              <input type="submit" value="Simpan" class="gradient-button" id="submit"/>
              <a href="#">Cancel</a>
            </form>
          </div>
          @foreach($galleries as $key => $gallery)
          <article class="one-fourth">
  					<figure><a href="#" title=""><img src="{{asset('template/images/gallery/'.$gallery->img_gallery)}}" alt="" width="270" height="152" /></a></figure>
  					<div class="details">
  						<h4>{{$gallery->title}}</h4>
              <a href="{{route('admin-gallery-anak',$gallery->id)}}" title="Detail" class="gradient-button">Detail</a>
              <a href="#edit{{$key}}" title="Edit" class="gradient-button edit">Edit</a>
              <div class="edit_field" id="edit{{$key}}">
                <form class="" action="{{route('admin-gallery')}}" method="post" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" name="action" value="editinduk" required>
                  <input type="hidden" name="ids" value="{{$gallery->id}}">
                  <label for="new_name">Title Gallery:</label>
                  <input type="text" name="title" value="{{$gallery->title}}"/ required>
                  <label for="new_name">Gambar:</label>
                  <input type="file" name="gambar"/>
                  <input type="submit" value="Simpan" class="gradient-button"/>
                  <a href="#">Cancel</a>
                </form>
              </div>
            </div>

  				</article>
          @endforeach
          </div>
      </section>

      <!--three-fourth content-->

    </div>
    <!--//main content-->
  </div>
</div>
@endsection
