@extends('layouts.admin_app')

@section('content')
<div class="main" role="main">
  <div class="wrap clearfix">
    <!--main content-->
    <div class="content clearfix">
      <!--breadcrumbs-->
      <nav role="navigation" class="breadcrumbs clearfix">
        <!--crumbs-->
        <ul class="crumbs">
          <li><a href="{{route('home')}}" title="Home">Home</a></li>
          <li><a href="{{route('admin-settings')}}" title="Settings">Settings</a></li>
          <li>Page</li>
        </ul>
      </nav>
      <section class="three-fourth">
        <form method="post" action="{{route('admin-settings-blog-edit',$blogs->id)}}" class="booking">
          @csrf
          <input type="hidden" name="action" value="simpan">
          <fieldset>
            <h3><span>EDIT </span>PAGE</h3>
            <div class="row">
                <!-- <h6>Title Blog :</h6> -->
                <input type="hidden" name="title" value="{{$blogs->title}}"/>
              <br><br>
                <h6>Description :</h6>
                <textarea rows="50" cols="10" id="summernote" name="deskripsi">{{$blogs->description}}</textarea>
            </div>
            <input type="submit" class="gradient-button" value="Save Blog" id="Save Blog" />
          </fieldset>
        </form>
      </section>

      @include('includes.right_sidebar')

    </div>
  </div>
</div>
@endsection
