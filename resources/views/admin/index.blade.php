@extends('layouts.admin_app')

@section('content')
<div class="main" role="main">
  <div class="wrap clearfix">
    <!--main content-->
    <div class="content clearfix">
      <!--breadcrumbs-->
      <nav role="navigation" class="breadcrumbs clearfix">
        <!--crumbs-->
        <ul class="crumbs">
          <li><a href="#" title="Home">Home</a></li>
        </ul>
        <!--//crumbs-->

      </nav>
    </div>
  </div>
</div>
@endsection
