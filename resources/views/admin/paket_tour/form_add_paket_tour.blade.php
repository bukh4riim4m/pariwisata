@extends('layouts.admin_app')

@section('content')
<div class="main" role="main">
  <div class="wrap clearfix">
    <!--main content-->
    <div class="content clearfix">
      <!--breadcrumbs-->
      <nav role="navigation" class="breadcrumbs clearfix">
        <!--crumbs-->
        <ul class="crumbs">
          <li><a href="{{route('home')}}" title="Home">Home</a></li>
          <li><a href="{{route('admin-paket-tour')}}" title="Paket Tour">Paket Tour</a></li>
          <li><a href="#" title="Form Add">Form Add</a></li>
        </ul>
        <ul class="top-right-nav">
					<!-- <li><a href="#" title="Back to results"><button  type="button" class="gradient-button" name="button">TAMBAH</button></a></li> -->
					<li><a href="{{route('admin-paket-tour')}}" title="Back">Back</a></li>
				</ul>
        <!--//crumbs-->

      </nav>
      <!--three-fourth content-->
        <section class="three-fourth">
          <form id="booking" method="post" action="{{route('admin-add-tour')}}" class="booking" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="action" value="add">
            <fieldset>
              <h3><span>ADD </span>PAKET TOUR</h3>
              <div class="row">
                <div class="f-item">
                  <label for="card_number">Nama Paket Tour</label>
                  <input type="text" name="nama_paket" required/>
                </div>
                <span class="info">Pax diisi dengan teks</span>
              </div>
              <div class="row twins">
                <div class="f-item">
                  <label for="card_number">Minimal Pax</label>
                  <input type="number" name="min_pax" required/>
                </div>
                <div class="f-item">
                  <label for="card_number">Maksimal Pax</label>
                  <input type="number" name="max_pax" required/>
                </div>
                <span class="info">Pax diisi dengan nomor</span>
              </div>

              <div class="row twins">
                <div class="f-item">
                  <label for="card_holder">Harga</label>
                  <input type="text" id="card_holder" name="harga" required />
                </div>
                <div class="f-item">
                  <label for="card_holder">Lokasi</label>
                  <input type="text" id="card_holder" name="lokasi" required />
                </div>
                <span class="info">Lokasi embet google maps</span>
              </div>
              <div class="row">
                <div class="f-item">
                  <label for="">Gambar</label>
                  <input type="file" name="gambar" value="" required>
                </div>
                <div class="f-item">
                  <label for="">Description</label>
                  <textarea name="deskripsi" id="summernote" rows="8" cols="80" required></textarea>
                  <!-- <textarea id="summernote" name="deskripsi" rows="8" cols="80"></textarea> -->
                </div>
                <span class="info">Deskripsi lengkap tentang paket</span>
              </div>
              <hr />
              <input type="submit" class="gradient-button" value="S I M P A N" id="next-step" />
            </fieldset>
          </form>
        </section>
      <!--//three-fourth content-->

      <!--right sidebar-->
      @include('includes.right_sidebar')
      <!--//right sidebar-->
    </div>
  </div>
</div>
@endsection
