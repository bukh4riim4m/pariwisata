@extends('layouts.admin_app')

@section('content')
<div class="main" role="main">
  <div class="wrap clearfix">
    <!--main content-->
    <div class="content clearfix">
      <!--breadcrumbs-->
      <nav role="navigation" class="breadcrumbs clearfix">
        <!--crumbs-->
        <ul class="crumbs">
          <li><a href="{{route('home')}}" title="Home">Home</a></li>
          <li><a href="#" title="Paket Tour">Paket Tour</a></li>
        </ul>
        <ul class="top-right-nav">
					<li><a href="{{route('admin-add-tour')}}" title="Tambah Paket"><button  type="button" class="gradient-button" name="button">TAMBAH</button></a></li>
				</ul>
        <!--//crumbs-->

      </nav>
    </div>
    <div class="main" role="main">
  		<div class="wrap clearfix">
  			<!--deals-->
  			<section class="full">
  				<div class="deals clearfix">
  					<!--deal-->
            @foreach($pakets as $key => $paket)
            <?php $gambars = App\Gambar::where('paket_id',$paket->id)->first(); ?>
  					<article class="one-fourth">
  						<figure><a href="hotel.html" title=""><img src="{{asset('template/images/uploads/'.$gambars->gambar)}}" alt="" width="270" height="152" /></a></figure>
  						<div class="details">
  							<h1>{{$paket->nama_paket}}
  								<span class="stars">
  									<img src="{{asset('template/images/ico/star.png')}}" alt="" />
  									<img src="{{asset('template/images/ico/star.png')}}" alt="" />
  									<img src="{{asset('template/images/ico/star.png')}}" alt="" />
  									<img src="{{asset('template/images/ico/star.png')}}" alt="" />
  								</span>
  							</h1>
  							<span class="address">Indonesia  •  </span>
  							<span class="rating"> 9 /10</span>
  							<span class="price">Harga / Pax  <em> IDR {{number_format($paket->harga,0,",",".")}}</em> </span>
  							<!-- <div class="description">
  								<p>{{$paket->description}} <a href="hotel.html">More info</a></p>
  							</div> -->
                <a href="{{route('admin-detail-tour',$paket->id)}}" title="Detail Paket Tour" class="gradient-button">Detail</a>
                <a href="#ganti{{$key}}" title="Edit" class="gradient-button edit">Edit</a>
                <div class="edit_field" id="ganti{{$key}}">
                  <form class="" action="{{route('admin-paket-tour')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="action" value="editpaket" required>
                    <input type="hidden" name="ids" value="{{$paket->id}}">
                    <label for="new_name">Nama Paket:</label>
                    <input type="text" name="nama_paket" value="{{$paket->nama_paket}}"/ required>
                    <label for="new_name">Harga Paket:</label>
                    <input type="text" name="harga" value="{{$paket->harga}}"/ required>
                    <label for="new_name">Lokasi Maps:</label>
                    <input type="text" name="lokasi" value="{{'<iframe src="'.$paket->lokasi.'" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>'}}"/ required>
                    <label for="new_name">Gambar:</label>
                    <input type="file" name="gambar"/>
                    <input type="submit" value="Simpan" class="gradient-button"/>
                    <a href="#">Cancel</a>
                  </form>
                </div>
              </div>
  					</article>
            @endforeach
  					<!--//deal-->
          </div>
        </section>
      </div>
    </div>
  </div>
</div>
@endsection
