@extends('layouts.admin_app')

@section('content')
<div class="main" role="main">
  <div class="wrap clearfix">
    <!--main content-->
    <div class="content clearfix">
      <!--breadcrumbs-->
      <nav role="navigation" class="breadcrumbs clearfix">
        <!--crumbs-->
        <ul class="crumbs">
          <li><a href="{{route('home')}}" title="Home">Home</a></li>
          <li><a href="#" title="Settings">Paket Tour</a></li>
          <li><a href="#" title="Settings">Detail</a></li>
        </ul>
        <ul class="top-right-nav">
					<!-- <li><a href="search_results.html" title="Back to results"><button  type="button" class="gradient-button" name="button">TAMBAH</button></a></li> -->
					<li> <strong><a href="{{route('admin-slider')}}" title="Back to results">Kembali</a></strong> </li>
				</ul>
        <!--//crumbs-->
      </nav>
      <section class="three-fourth">
        <div class="gallery">
						<div class="lSSlideOuter ">
              <div class="lSSlideWrapper usingCss" style="transition-duration: 500ms; transition-timing-function: ease;">
                <ul id="image-gallery" class="lightSlider lsGrab lSSlide" style="width: 6980px; transform: translate3d(-3490px, 0px, 0px); height: 581.656px; padding-bottom: 0%;">
                  <?php $gambars = App\Gambar::where('paket_id',$pakets->id)->where('active',1)->get(); ?>
                  @foreach($gambars as $key => $gambar)
                  <li data-thumb="{{asset('template/images/uploads/'.$gambar->gambar)}}" class="lslide" style="width: 872.5px; margin-right: 0px;">
    								<img src="{{asset('template/images/uploads/'.$gambar->gambar)}}" alt="">
    							</li>
                  @endforeach
                </ul>
              <div class="lSAction">
                <a class="lSPrev"></a><a class="lSNext"></a>
              </div>
            </div>
                <!-- <ul class="lSPager lSGallery" style="margin-top: 5px; transition-duration: 500ms; width: 878px; transform: translate3d(-0.5px, 0px, 0px);">
                  <li style="width:100%;width:141.25px;margin-right:5px" class="">
                    <a href="#"><img src="{{asset('template/images/uploads/hotel1.jpg')}}"></a>
                  </li>
                  <li style="width:100%;width:141.25px;margin-right:5px">
                    <a href="#"><img src="{{asset('template/images/uploads/hotel1.jpg')}}"></a>
                  </li>
                  <li style="width:100%;width:141.25px;margin-right:5px">
                    <a href="#"><img src="{{asset('template/images/uploads/hotel1.jpg')}}"></a>
                  </li>
                  <li style="width:100%;width:141.25px;margin-right:5px" class="active">
                    <a href="#"><img src="{{asset('template/images/uploads/hotel1.jpg')}}"></a>
                  </li>
                  <li style="width:100%;width:141.25px;margin-right:5px">
                    <a href="#"><img src="{{asset('template/images/uploads/hotel1.jpg')}}"></a>
                  </li>
                  <li style="width:100%;width:141.25px;margin-right:5px">
                    <a href="#"><img src="{{asset('template/images/uploads/hotel1.jpg')}}"></a>
                  </li>
                </ul> -->
              </div>
					</div>
        <!--gallery-->
        <!-- <section class="gallery" id="crossfade">
          <img src="{{asset('template/images/slider/img.jpg')}}" alt="" width="850" height="531" />
          <img src="{{asset('template/images/slider/img.jpg')}}" alt="" width="850" height="531" />
          <img src="{{asset('template/images/slider/img.jpg')}}" alt="" width="850" height="531" />
          <img src="{{asset('template/images/slider/img.jpg')}}" alt="" width="850" height="531" />
        </section> -->
        <!--//gallery-->

        <!--inner navigation-->
        <nav class="inner-nav">
          <ul>
            <li class="availability"><a href="#availability" title="Gallery">Gallery</a></li>
            <li class="description"><a href="#description" title="Description">Description</a></li>
            <li class="facilities"><a href="#facilities" title="Facilities">Facilities</a></li>
            <li class="location"><a href="#location" title="Location">Location</a></li>
            <li class="reviews"><a href="#reviews" title="Pax">Pax</a></li>
            <!-- <li class="things-to-do"><a href="#things-to-do" title="Things to do">Things to do</a></li> -->
          </ul>
        </nav>
        <!--//inner navigation-->

        <!--availability-->
        <section id="availability" class="tab-content">
          <article>
            <h1>Gallery</h1>
            <div class="text-wrap">
              <a href="#field" class="gradient-button edit">TAMBAH GAMBAR</a>
            </div>
            <div class="edit_field" id="field">
              <form class="" action="{{route('admin-detail-tour',$pakets->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="action" value="add" required>
                <label for="new_name">Gambar:</label>
                <input type="file" name="gambar" required/>
                <input type="submit" value="Simpan" class="gradient-button"/>
                <a href="#">Cancel</a>
              </form>
            </div>
            <hr>
            <h1>Gallery</h1>
            <ul class="room-types">
              <!--room-->


              @foreach($gambars as $key => $gambar)
              <li>
                <figure class="left"><img src="{{asset('template/images/uploads/'.$gambar->gambar)}}" alt="" width="270" height="152" /><a href="images/slider/img.jpg')}}" class="image-overlay" rel="prettyPhoto[gallery1]"></a></figure>
                <div>
                  <a href="#logo{{$key}}" class="gradient-button edit">GANTI</a>
                  <a href="#hapus{{$key}}" class="gradient-button edit">HAPUS</a>
                </div>
                <div class="edit_field" id="logo{{$key}}">
                  <form class="" action="{{route('admin-detail-tour',$gambar->id)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="action" value="edit">
                    <input type="file" name="gambar"/>
                    <input type="submit" value="Simpan" class="gradient-button"/>
                    <a href="#">Cancel</a>
                  </form>
                </div>
                <div class="edit_field" id="hapus{{$key}}">
                  <form class="" action="{{route('admin-detail-tour',$gambar->id)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="action" value="hapus">
                    <p>Yakin di Hapus ???</p>
                    <input type="submit" value="YA" class="gradient-button"/>
                    <a href="#hapus{{$key}}" class="gradient-button edit">Tidak</a>
                  </form>
                </div>
              </li>
              <!--//room-->
              @endforeach
              <!--room-->
              {{--<li>
                <figure class="left"><img src="{{asset('template/images/slider/img.jpg')}}" alt="" width="270" height="152" /><a href="images/slider/img.jpg')}}" class="image-overlay" rel="prettyPhoto[gallery1]"></a></figure>
                <div class="meta">
                  <h2>Deluxe Single Room</h2>
                  <p>Prices are per room<br />20 % VAT Included in price</p>
                  <p>Non-refundable<br />Full English breakfast $ 24.80 </p>
                  <a href="javascript:void(0)" title="more info" class="more-info">+ more info</a>
                </div>
                <div class="room-information">
                  <div class="row">
                    <span class="first">Max:</span>
                    <span class="second"><img src="{{asset('template/images/ico/person.png')}}" alt="" /></span>
                  </div>
                  <div class="row">
                    <span class="first">Price:</span>
                    <span class="second">$ 55</span>
                  </div>
                  <div class="row">
                    <span class="first">Rooms:</span>
                    <span class="second">01</span>
                  </div>
                  <a href="booking-step1.html" class="gradient-button" title="Book">Book</a>
                </div>
                <div class="more-information">
                  <p>Stylish and individually designed room featuring a satellite TV, mini bar and a 24-hour room service menu.</p>
                  <p><strong>Room Facilities:</strong> Safety Deposit Box, Air Conditioning, Desk, Ironing Facilities, Seating Area, Heating, Shower, Bath, Hairdryer, Toilet, Bathroom, Pay-per-view Channels, TV, Telephone</p>
                  <p><strong>Bed Size(s):</strong> 1 Double </p>
                  <p><strong>Room Size:</strong>  16 square metres</p>
                </div>
              </li>
              <!--//room-->

              <!--room-->
              <li>
                <figure class="left"><img src="{{asset('template/images/slider/img.jpg')}}" alt="" width="270" height="152" /><a href="images/slider/img.jpg')}}" class="image-overlay" rel="prettyPhoto[gallery1]"></a></figure>
                <div class="meta">
                  <h2>Standard Family Room</h2>
                  <p>Prices are per room<br />20 % VAT Included in price</p>
                  <p>Non-refundable<br />Full English breakfast $ 24.80 </p>
                  <a href="javascript:void(0)" title="more info" class="more-info">+ more info</a>
                </div>
                <div class="room-information">
                  <div class="row">
                    <span class="first">Max:</span>
                    <span class="second"><img src="{{asset('template/images/ico/person.png')}}" alt="" /><img src="{{asset('template/images/ico/person.png')}}" alt="" /><img src="{{asset('template/images/ico/person.png')}}" alt="" /></span>
                  </div>
                  <div class="row">
                    <span class="first">Price:</span>
                    <span class="second">$ 55</span>
                  </div>
                  <div class="row">
                    <span class="first">Rooms:</span>
                    <span class="second">01</span>
                  </div>
                  <a href="booking-step1.html" class="gradient-button" title="Book">Book</a>
                </div>
                <div class="more-information">
                  <p>Stylish and individually designed room featuring a satellite TV, mini bar and a 24-hour room service menu.</p>
                  <p><strong>Room Facilities:</strong> Safety Deposit Box, Air Conditioning, Desk, Ironing Facilities, Seating Area, Heating, Shower, Bath, Hairdryer, Toilet, Bathroom, Pay-per-view Channels, TV, Telephone</p>
                  <p><strong>Bed Size(s):</strong> 1 Double </p>
                  <p><strong>Room Size:</strong>  16 square metres</p>
                </div>
              </li>
              <!--//room-->

              <!--room-->
              <li>
                <figure class="left"><img src="{{asset('template/images/slider/img.jpg')}}" alt="" width="270" height="152" /><a href="images/slider/img.jpg')}}" class="image-overlay" rel="prettyPhoto[gallery1]"></a></figure>
                <div class="meta">
                  <h2>Superior Double Room</h2>
                  <p>Prices are per room<br />20 % VAT Included in price</p>
                  <p>Non-refundable<br />Full English breakfast $ 24.80 </p>
                  <a href="javascript:void(0)" title="more info" class="more-info">+ more info</a>
                </div>
                <div class="room-information">
                  <div class="row">
                    <span class="first">Max:</span>
                    <span class="second"><img src="{{asset('template/images/ico/person.png')}}" alt="" /><img src="{{asset('template/images/ico/person.png')}}" alt="" /></span>
                  </div>
                  <div class="row">
                    <span class="first">Price:</span>
                    <span class="second">$ 55</span>
                  </div>
                  <div class="row">
                    <span class="first">Rooms:</span>
                    <span class="second">01</span>
                  </div>
                  <a href="booking-step1.html" class="gradient-button" title="Book">Book</a>
                </div>
                <div class="more-information">
                  <p>Stylish and individually designed room featuring a satellite TV, mini bar and a 24-hour room service menu.</p>
                  <p><strong>Room Facilities:</strong> Safety Deposit Box, Air Conditioning, Desk, Ironing Facilities, Seating Area, Heating, Shower, Bath, Hairdryer, Toilet, Bathroom, Pay-per-view Channels, TV, Telephone</p>
                  <p><strong>Bed Size(s):</strong> 1 Double </p>
                  <p><strong>Room Size:</strong>  16 square metres</p>
                </div>
              </li>
              <!--//room-->--}}
            </ul>
          </article>
        </section>
        <!--//availability-->

        <!--description-->
        <section id="description" class="tab-content">
          <article>
            <h1>Description</h1>
            <div class="text-wrap">
              <p>{!!$pakets->description!!}</p>
            </div>
            <div>
              <a href="#edit{{$pakets->id}}" class="gradient-button edit">GANTI</a>
            </div>
            <div class="edit_field" id="edit{{$pakets->id}}">
              <form class="" action="{{route('admin-detail-tour-description',$pakets->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="action" value="description">
                <textarea name="deskripsi" id="summernote" rows="8" cols="80">{{$pakets->description}}</textarea>
                <input type="submit" value="Simpan" class="gradient-button"/>
                <a href="#">Cancel</a>
              </form>
            </div>
          </article>
        </section>
        <!--//description-->

        <!--facilities-->
        <section id="facilities" class="tab-content">
          <article class="mysettings">
            <h1>Facilities <a href="#tambahfacilities" class="gradient-button edit right">Tambah</a></h1>
            <div class="edit_field" id="tambahfacilities">
              <label for="new_name">Tambah Facilities:</label>
              <form class="" action="{{route('admin-detail-tour-description',$pakets->id)}}" method="post">
                @csrf
                <input type="hidden" name="action" value="facilititambah">
                <input type="text" name="faciliti" value="" required/>
                <input type="submit" value="save" class="gradient-button" id="submit1"/>
                <a href="#">Cancel</a>
              </form>
            </div>
            <table>
              @foreach($facilities as $key => $faciliti)
              <tr>
                <!-- <th>First name:</th> -->
                <td>{{$key+1}}. {{$faciliti->name}}
                  <!--edit fields-->
                  <div class="edit_field" id="editfaciliti{{$key}}">
                    <label for="new_name">Edit Facilities:</label>
                    <form class="" action="{{route('admin-detail-tour-description',$faciliti->id)}}" method="post">
                      @csrf
                      <input type="hidden" name="action" value="facilitiedit">
                      <input type="text" name="faciliti" value="{{$faciliti->name}}" required/>
                      <input type="submit" value="save" class="gradient-button" id="submit1"/>
                      <a href="#">Cancel</a>
                    </form>
                  </div>
                  <!--//edit fields-->
                </td>
                <td style="width:150px;"><a href="#editfaciliti{{$key}}" class="gradient-button edit">Ganti</a> <a href="#field{{$key}}" class="gradient-button edit">Hapus</a></td>
              </tr>
              @endforeach
            </table>

          </article>
        </section>
        <!--//facilities-->

        <!--location-->
        <section id="location" class="tab-content">
          <article>
            <!--map-->

            <iframe src="{{$pakets->lokasi}}" width="100%" height="500px" frameborder="0" style="border:0" allowfullscreen></iframe>
              <!-- <div class="gmap" id="map_canvas"></div> -->
            <!--//map-->
          </article>
        </section>
        <!--//location-->

        <!--reviews-->
        <section id="reviews" class="tab-content">
          <article>
            <h1>Pax</h1>
            <div>
              <h5>Minimal &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : {{$pakets->min_pax}} Pax</h5>
              <h5>Maksimal &nbsp;&nbsp;&nbsp; : {{$pakets->max_pax}} Pax</h5>
            </div>
            <div class="right">
              <a href="#editpax" class="gradient-button edit">Edit</a>
            </div>
            <div class="edit_field" id="editpax">
              <h2>Edit Jumlah Pax</h2>
              <form class="" action="{{route('admin-tour-edit-pax',$pakets->id)}}" method="post">
                @csrf
                <input type="hidden" name="action" value="editpax">
                <label for="">Minimal Pax</label>
                <input type="number" name="min_pax" value="{{$pakets->min_pax}}" required/>
                <label for="">Maksimal Pax</label>
                <input type="number" name="max_pax" value="{{$pakets->max_pax}}" required/>
                <input type="submit" value="save" class="gradient-button" id="submit1"/>
                <a href="#">Cancel</a>
              </form>
            </div>
          </article>
        </section>
        <!--//reviews-->

        <!--things to do-->
        <section id="things-to-do" class="tab-content">
          <article>
            <h1>London</h1>
            <figure class="left_pic"><img src="{{asset('template/images/uploads/img.jpg')}}" alt="Things to do - London general" /></figure>
            <p class="teaser">London is a diverse and exciting city with some of the best sights and attractions in the world. </p>
            <p>See London from above on the London Eye; meet a celebrity at Madame Tussauds; examine some of the world’s most precious treasures at the British Museum or come face-to-face with the dinosaurs at the Natural History Museum.</p>

            <h1>Sports and nature</h1>
            <figure class="left_pic"><img src="{{asset('template/images/uploads/img.jpg')}}" alt="Things to do - London Sports and nature" /></figure>
            <p class="teaser">London is one of the greenest capitals in the world, with plenty of green and open spaces. There are more than 3000 open spaces.</p>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.<br />Ut wisi enim ad minim veniam, quis nostrud exerci. </p>

            <h1>Nightlife</h1>
            <figure class="left_pic"><img src="{{asset('template/images/uploads/img.jpg')}}" alt="Things to do - London Nightlife" /></figure>
            <p class="teaser">Looking for nightclubs in London? Take a look at our guide to London clubs. Browse for club ideas, regular club nights and one-off events. </p>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.<br />Ut wisi enim ad minim veniam, quis nostrud exerci. </p>

            <h1>Culture and history</h1>
            <figure class="left_pic"><img src="{{asset('template/images/uploads/img.jpg')}}" alt="Things to do - London general" /></figure>
            <p class="teaser">For a display of British pomp and ceremony, watch the Changing the Guard ceremony outside Buckingham Palace.</p>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.<br />Ut wisi enim ad minim veniam, quis nostrud exerci. </p>
            <hr />
            <a href="#" class="gradient-button right" title="Read more">Read more</a>
          </article>
        </section>
        <!--//things to do-->
      </section>
        @include('includes.right_sidebar')

    </div>
  </div>
</div>
@endsection
