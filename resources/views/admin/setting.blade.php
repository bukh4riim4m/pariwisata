@extends('layouts.admin_app')

@section('content')
<?php $contacts = App\Web::find(1); ?>
<div class="main" role="main">
  <div class="wrap clearfix">
    <!--main content-->
    <div class="content clearfix">
      <!--breadcrumbs-->
      <nav role="navigation" class="breadcrumbs clearfix">
        <!--crumbs-->
        <ul class="crumbs">
          <li><a href="{{route('home')}}" title="Home">Home</a></li>
          <li><a href="#" title="Settings">Settings</a></li>
        </ul>
      </nav>
      <section class="three-fourth">
        <!--inner navigation-->
        <nav class="inner-nav">
          <ul>
            <li class="availability"><a href="#availability" title="Sliders">Sliders</a></li>
            <li class="description"><a href="#facilities" title="About">About</a></li>
            <li class="description"><a href="#description" title="Faq">Faq</a></li>
            <li class="description"><a href="#booking" title="Booking Tips">Booking Tips</a></li>
            <li class="description"><a href="#payment" title="Payment Option">Payment Option</a></li>
            <li class="description"><a href="#reservation" title="Reservation">Reservation</a></li>
            <li class="location"><a href="#location" title="Contact">Contact</a></li>


          </ul>
        </nav>
        <!--//inner navigation-->

        <!--availability-->
        <section id="availability" class="tab-content">
          <article>
            <h1>Sliders</h1>

            <ul class="room-types">
              <!--room-->
              <li>
                <!-- <div class="room-information"> -->
                  <div class="row">
                      <a href="#field" class="gradient-button edit">ADD SLIDER</a>
                      <a href="#logo" class="gradient-button edit">EDIT LOGO</a>
                  </div>
                <!-- </div> -->
                <!-- <p>Available rooms from <span class="date">Thurs 29 Nov 2012</span> to <span class="date">Fri 30 Nov 2012</span>.</p> -->
                <div class="edit_field" id="field">
                  <form class="" action="{{route('admin-settings-slider')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="action" value="add" required>
                    <label for="new_name">Title Slide:</label>
                    <input type="text" name="title" value=""/ required>
                    <label for="new_name">Description:</label>
                    <input type="text" name="deskripsi" value=""/ required>
                    <label for="new_name">Gambar:</label>
                    <input type="file" name="gambar" required/>
                    <input type="submit" value="Simpan" class="gradient-button" id="submit"/>
                    <a href="#">Cancel</a>
                  </form>
                </div>
                <div class="edit_field" id="logo">
                  <form class="" action="{{route('admin-settings-logo')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <?php $webs = App\Web::find(1); ?>
                    <img src="{{asset('template/images/txt/'.$webs->logo)}}" />
                    <hr>
                    <label for="new_name">Title Website:</label>
                    <input type="text" name="title" value="{{$webs->title_web}}">
                    <label for="new_name">LOGO 224 x 50 :</label>
                    <input type="file" name="gambar"/>
                    <input type="submit" value="Simpan" class="gradient-button" id="submit"/>
                    <a href="#">Cancel</a>
                  </form>
                </div>
              </li>
              @foreach($sliders as $key=>$slide)
              <li>
                <figure class="left"><img src="{{asset('template/images/slider/'.$slide->gambar)}}" alt="" width="270" height="152" /><a href="images/slider/img.jpg" class="image-overlay" rel="prettyPhoto[gallery1]"></a></figure>
                <div class="meta">
                  <!-- <h2>{{$slide->title}}</h2> -->
                  <p style="text-align:justify;">{{$slide->description}}</p>
                  <!-- <a href="javascript:void(0)" title="more info" class="more-info">+ more info</a> -->
                </div>

                <div class="right">
                  <div class="row">
                    <a href="#field{{$key}}" class="gradient-button edit">Ganti</a>
                  </div>
                </div>
                <div class="right" style="margin:top:2px;">
                  <div class="row"><br>
                    <a href="#field{{$key}}-{{$key}}" class="gradient-button edit">Hapus</a>
                  </div>
                </div><hr>
                <div class="row">
                  <th width="100%"></th>
                  <div class="edit_field" id="field{{$key}}">
                    <form class="" action="{{route('admin-settings-slider')}}" method="post" enctype="multipart/form-data">
                      @csrf
                      <input type="hidden" name="action" value="edit">
                      <input type="hidden" name="ids" value="{{$slide->id}}" required>
                      <label for="new_name">Title Slide:</label>
                      <input type="text" name="title" value="{{$slide->title}}" required/>
                      <label for="new_name">Description:</label>
                      <input type="text" name="deskripsi" value="{{$slide->description}}" required/>
                      <label for="new_name">Gambar:</label>
                      <input type="file" name="gambar" class="form-control"/>
                      <input type="submit" value="Simpan" class="gradient-button" id="submit1"/>
                      <a href="#">Batal</a>
                    </form>

                  </div>
                  <div class="edit_field" id="field{{$key}}-{{$key}}">
                    <form class="" action="{{route('admin-settings-slider')}}" method="post" enctype="multipart/form-data">
                      @csrf
                      <input type="hidden" name="action" value="hapus">
                      <input type="hidden" name="ids" value="{{$slide->id}}">
                      <label for="new_name">Yakin dihapus ?</label><br>
                      <input type="submit" value="Hapus" class="gradient-button" id="submit1"/>
                      <a href="#">Batal</a>
                    </form>

                  </div>
                </div>

              </li>
              @endforeach
              <!--//room-->
            </ul>
          </article>
        </section>
        <!--//availability-->
        <!--facilities-->
        <section id="facilities" class="tab-content">
          @foreach($abouts as $key => $about)

					<article class="static-content post" style="background-color:#ffffff;margin-bottom:15px;padding:20px;">
            	<!-- <h2>{{$about->title}}</h2> -->
							<!-- <p class="entry-meta"> -->
								<span class="date">Date: {{date('d M Y H:i', strtotime($about->created_at))}}</span>
							<!-- </p> -->
						<!-- <div class="entry-content"> -->
							<p>{!!$about->description!!}</p>
							<!-- <a href="#hapusabout{{$key}}" class="gradient-button edit right">Hapus</a> -->
              <a href="{{route('admin-settings-blog-edit',$about->id)}}" class="gradient-button right">Edit</a>

						<!-- </div> -->

            <!-- <div class="edit_field" id="hapusabout{{$key}}">
              <form class="" action="{{route('admin-settings-blog-edit',$about->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                <label for=""> Yakin akan di hapus ? </label>
                <input type="hidden" name="action" value="hapus" required>
                <hr>
                <input type="submit" value="Ya" class="gradient-button" id="submit"/>
                <a href="#">Tidak</a>
              </form>
            </div> -->
					</article>
          @endforeach
          @if(count($abouts) < 1)
          <article class="static-content post" style="background-color:#ffffff;margin-bottom:15px;padding:10px;">
            <div class="entry-content">
              <p>Belum ada postingan</p>
              <a href="#addabout" class="gradient-button edit">Posting Baru</a>
            </div>
            <div class="edit_field" id="addabout">
              <form class="" action="{{route('admin-settings-blog')}}" method="post" enctype="multipart/form-data">
                @csrf
                <label for="">Title</label>
                <input type="hidden" name="action" value="add" required>
                <input type="hidden" name="page" value="about" required>
                <input type="text" name="title" value="" required>
                <label for="">Description</label>
                <textarea name="description" id="summernote" rows="8" cols="80"></textarea>
                <hr>
                <input type="submit" value="Simpan" class="gradient-button" id="submit"/>
                <a href="#">Cancel</a>
              </form>
            </div>
          </article>
          @endif
        </section>
        <!--//facilities-->
        <!--description-->
        <section id="description" class="tab-content">

          @foreach($faqs as $key => $faq)

					<article class="static-content post" style="background-color:#ffffff;margin-bottom:15px;padding:20px;">
            	<!-- <h2>{{$faq->title}}</h2> -->
							<!-- <p class="entry-meta"> -->
								<span class="date">Date: {{date('d M Y H:i', strtotime($faq->created_at))}}</span>
							<!-- </p> -->
						<!-- <div class="entry-content"> -->
							<p>{!!$faq->description!!}</p>
							<!-- <a href="#hapusfaq{{$key}}" class="gradient-button edit right">Hapus</a> -->
              <a href="{{route('admin-settings-blog-edit',$faq->id)}}" class="gradient-button right">Edit</a>

						<!-- </div> -->

            <!-- <div class="edit_field" id="hapusfaq{{$key}}">
              <form class="" action="{{route('admin-settings-blog-edit',$faq->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                <label for=""> Yakin akan di hapus ? </label>
                <input type="hidden" name="action" value="hapus" required>
                <hr>
                <input type="submit" value="Ya" class="gradient-button" id="submit"/>
                <a href="#">Tidak</a>
              </form>
            </div> -->
					</article>
          @endforeach
          @if(count($faqs) < 1)
          <article class="static-content post" style="background-color:#ffffff;margin-bottom:15px;padding:10px;">
            <div class="entry-content">
              <p>Belum ada postingan</p>
              <a href="#addfaq" class="gradient-button edit">Posting Baru</a>
            </div>
            <div class="edit_field" id="addfaq">
              <form class="" action="{{route('admin-settings-blog')}}" method="post" enctype="multipart/form-data">
                @csrf
                <label for="">Title</label>
                <input type="hidden" name="action" value="add" required>
                <input type="hidden" name="page" value="faq" required>
                <input type="text" name="title" value="" required>
                <label for="">Description</label>
                <textarea name="description" id="summernote" rows="8" cols="80"></textarea>
                <hr>
                <input type="submit" value="Simpan" class="gradient-button" id="submit"/>
                <a href="#">Cancel</a>
              </form>
            </div>
          </article>
          @endif
        </section>
        <!--//description-->
        <!--description-->
        <section id="booking" class="tab-content">

          @foreach($bookingtips as $key => $bookingtip)

          <article class="static-content post" style="background-color:#ffffff;margin-bottom:15px;padding:20px;">
              <!-- <h2>{{$bookingtip->title}}</h2> -->
              <!-- <p class="entry-meta"> -->
                <span class="date">Date: {{date('d M Y H:i', strtotime($bookingtip->created_at))}}</span>
              <!-- </p> -->
            <!-- <div class="entry-content"> -->
              <p>{!!$bookingtip->description!!}</p>
              <!-- <a href="#hapusbook{{$key}}" class="gradient-button edit right">Hapus</a> -->
              <a href="{{route('admin-settings-blog-edit',$bookingtip->id)}}" class="gradient-button right">Edit</a>

            <!-- </div> -->

            <!-- <div class="edit_field" id="hapusbook{{$key}}">
              <form class="" action="{{route('admin-settings-blog-edit',$bookingtip->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                <label for=""> Yakin akan di hapus ? </label>
                <input type="hidden" name="action" value="hapus" required>
                <hr>
                <input type="submit" value="Ya" class="gradient-button" id="submit"/>
                <a href="#">Tidak</a>
              </form>
            </div> -->
          </article>
          @endforeach
          @if(count($bookingtips) < 1)
          <article class="static-content post" style="background-color:#ffffff;margin-bottom:15px;padding:10px;">
            <div class="entry-content">
              <p>Belum ada postingan</p>
              <a href="#addbook" class="gradient-button edit">Posting Baru</a>
            </div>
            <div class="edit_field" id="addbook">
              <form class="" action="{{route('admin-settings-blog')}}" method="post" enctype="multipart/form-data">
                @csrf
                <label for="">Title</label>
                <input type="hidden" name="action" value="add" required>
                <input type="hidden" name="page" value="tips" required>
                <input type="text" name="title" value="" required>
                <label for="">Description</label>
                <textarea name="description" id="summernote" rows="8" cols="80"></textarea>
                <hr>
                <input type="submit" value="Simpan" class="gradient-button" id="submit"/>
                <a href="#">Cancel</a>
              </form>
            </div>
          </article>
          @endif
        </section>
        <!--description-->
        <section id="payment" class="tab-content">

          @foreach($paymenoptions as $key => $paymenoption)

          <article class="static-content post" style="background-color:#ffffff;margin-bottom:15px;padding:20px;">
              <!-- <h2>{{$paymenoption->title}}</h2> -->
              <!-- <p class="entry-meta"> -->
                <span class="date">Date: {{date('d M Y H:i', strtotime($paymenoption->created_at))}}</span>
              <!-- </p> -->
            <!-- <div class="entry-content"> -->
              <p>{!!$paymenoption->description!!}</p>
              <!-- <a href="#hapuspay{{$key}}" class="gradient-button edit right">Hapus</a> -->
              <a href="{{route('admin-settings-blog-edit',$paymenoption->id)}}" class="gradient-button right">Edit</a>

            <!-- </div> -->

            <!-- <div class="edit_field" id="hapuspay{{$key}}">
              <form class="" action="{{route('admin-settings-blog-edit',$paymenoption->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                <label for=""> Yakin akan di hapus ? </label>
                <input type="hidden" name="action" value="hapus" required>
                <hr>
                <input type="submit" value="Ya" class="gradient-button" id="submit"/>
                <a href="#">Tidak</a>
              </form>
            </div> -->
          </article>
          @endforeach
          @if(count($paymenoptions) < 1)
          <article class="static-content post" style="background-color:#ffffff;margin-bottom:15px;padding:10px;">
            <div class="entry-content">
              <p>Belum ada postingan</p>
              <a href="#addpay" class="gradient-button edit">Posting Baru</a>
            </div>
            <div class="edit_field" id="addpay">
              <form class="" action="{{route('admin-settings-blog')}}" method="post" enctype="multipart/form-data">
                @csrf
                <label for="">Title</label>
                <input type="hidden" name="action" value="add" required>
                <input type="hidden" name="page" value="payment" required>
                <input type="text" name="title" value="" required>
                <label for="">Description</label>
                <textarea name="description" id="summernote" rows="8" cols="80"></textarea>
                <hr>
                <input type="submit" value="Simpan" class="gradient-button" id="submit"/>
                <a href="#">Cancel</a>
              </form>
            </div>
          </article>
          @endif
        </section>
        <!--//description-->

        <!--//description-->
        <!--description-->
        <section id="reservation" class="tab-content">

          @foreach($reservations as $key => $reservation)

          <article class="static-content post" style="background-color:#ffffff;margin-bottom:15px;padding:20px;">
              <!-- <h2>{{$reservation->title}}</h2> -->
              <!-- <p class="entry-meta"> -->
                <span class="date">Date: {{date('d M Y H:i', strtotime($reservation->created_at))}}</span>
              <!-- </p> -->
            <!-- <div class="entry-content"> -->
              <p>{!!$reservation->description!!}</p>
              <!-- <a href="#hapusreservation{{$key}}" class="gradient-button edit right">Hapus</a> -->
              <a href="{{route('admin-settings-blog-edit',$reservation->id)}}" class="gradient-button right">Edit</a>

            <!-- </div> -->

            <!-- <div class="edit_field" id="hapusreservation{{$key}}">
              <form class="" action="{{route('admin-settings-blog-edit',$reservation->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                <label for=""> Yakin akan di hapus ? </label>
                <input type="hidden" name="action" value="hapus" required>
                <hr>
                <input type="submit" value="Ya" class="gradient-button" id="submit"/>
                <a href="#">Tidak</a>
              </form>
            </div> -->
          </article>
          @endforeach
          @if(count($reservations) < 1)
          <article class="static-content post" style="background-color:#ffffff;margin-bottom:15px;padding:10px;">
            <div class="entry-content">
              <p>Belum ada postingan</p>
              <a href="#addreservation" class="gradient-button edit">Posting Baru</a>
            </div>
            <div class="edit_field" id="addreservation">
              <form class="" action="{{route('admin-settings-blog')}}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="action" value="add" required>
                <input type="hidden" name="page" value="reservation" required>
                <input type="hidden" name="title" value="About" required>
                <label for="">Description</label>
                <textarea name="description" id="summernote" rows="8" cols="80"></textarea>
                <hr>
                <input type="submit" value="Simpan" class="gradient-button" id="submit"/>
                <a href="#">Cancel</a>
              </form>
            </div>
          </article>
          @endif
        </section>
        <!--//description-->
        <!--location-->
        <section id="location" class="tab-content">
          <div class="row">
              <a href="#editmaps" class="gradient-button edit right">EDIT CONTACT</a>
          </div>
          <div class="edit_field" id="editmaps">
            <form class="" action="{{route('admin-setting-contact')}}" method="post">
              @csrf
              <label for="">Phone Number</label>
              <input type="text" name="telp" value="{{$contacts->telp}}" required>
              <label for="">Mobile Number</label>
              <input type="text" name="hp" value="{{$contacts->hp}}" required>
              <label for="">Email Address</label>
              <input type="email" name="email" value="{{$contacts->email}}" required>
              <label for="">Input Embed Maps</label>
              <input type="hidden" name="action" value="add" required>
              <input type="text" name="maps" value="{{'<iframe src="'.$contacts->maps.'" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>'}}" required>
              <hr>
              <input type="submit" value="Simpan" class="gradient-button" id="submit"/>
              <a href="#">Cancel</a>
            </form>
          </div>
          <br>
          <article>
            <!--map-->
            <iframe src="{{$contacts->maps}}" width="100%" height="500px" frameborder="0" style="border:0" allowfullscreen></iframe>
            <!--//map-->
          </article>
        </section>
        <!--//location-->

      </section>
      @include('includes.right_sidebar')

    </div>
  </div>
</div>
@endsection
