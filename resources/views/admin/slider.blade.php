@extends('layouts.admin_app')

@section('content')
<div class="main" role="main">
  <div class="wrap clearfix">
    <!--main content-->
    <div class="content clearfix">
      <!--breadcrumbs-->
      <nav role="navigation" class="breadcrumbs clearfix">
        <!--crumbs-->
        <ul class="crumbs">
          <li><a href="{{route('admin-home')}}" title="Home">Home</a></li>
          <li><a href="#" title="Slider">Slider</a></li>
        </ul>
        <ul class="top-right-nav">
					<li><a href="{{route('admin-slider-create')}}" title="Back to results"><button  type="button" class="gradient-button" name="button">TAMBAH</button></a></li>
					<!-- <li><a href="#" title="Change search">Change search</a></li> -->
				</ul>
        <!--//crumbs-->
      </nav>
      <section class="destinations clearfix first">
        <h1>Top Slider Website</h1>

        <!--column-->
        <article class="one-fourth">
          <figure><a href="location.html" title=""><img src="images/uploads/img.jpg" alt="" width="270" height="152" /></a></figure>
          <div class="details">
            <a href="location.html" title="View all" class="gradient-button">View all</a>
            <h5>Paris</h5>
            <span class="count">1529 Hotels</span>
            <div class="ribbon">
              <div class="half hotel">
                <a href="hotels.html" title="View all">
                  <span class="small">from</span>
                  <span class="price">&#36; 70</span>
                </a>
              </div>
              <div class="half flight">
                <a href="flights.html" title="View all">
                  <span class="small">from</span>
                  <span class="price">&#36; 150</span>
                </a>
              </div>
            </div>
          </div>
        </article>
        <!--//column-->

        {{--<!--column-->
        <article class="one-fourth">
          <figure><a href="location.html" title=""><img src="images/uploads/img.jpg" alt="" width="270" height="152" /></a></figure>
          <div class="details">
            <a href="location.html" title="View all" class="gradient-button">View all</a>
            <h5>Amsterdam</h5>
            <span class="count">929 Hotels</span>
            <div class="ribbon">
              <div class="half hotel">
                <a href="hotels.html" title="View all">
                  <span class="small">from</span>
                  <span class="price">&#36; 70</span>
                </a>
              </div>
              <div class="half flight">
                <a href="flights.html" title="View all">
                  <span class="small">from</span>
                  <span class="price">&#36; 150</span>
                </a>
              </div>
            </div>
          </div>
        </article>
        <!--//column-->

        <!--column-->
        <article class="one-fourth">
          <figure><a href="location.html" title=""><img src="images/uploads/img.jpg" alt="" width="270" height="152" /></a></figure>
          <div class="details">
            <a href="location.html" title="View all" class="gradient-button">View all</a>
            <h5>Saint Petersburg</h5>
            <span class="count">658 Hotels</span>
            <div class="ribbon">
              <div class="half hotel">
                <a href="hotels.html" title="View all">
                  <span class="small">from</span>
                  <span class="price">&#36; 70</span>
                </a>
              </div>
              <div class="half flight">
                <a href="flights.html" title="View all">
                  <span class="small">from</span>
                  <span class="price">&#36; 150</span>
                </a>
              </div>
            </div>
          </div>
        </article>
        <!--//column-->

        <!--column-->
        <article class="one-fourth promo last">
          <div class="ribbon-small">- 20%</div>
          <figure><a href="location.html" title=""><img src="images/uploads/img.jpg" alt="" width="270" height="152" /></a></figure>
          <div class="details">
            <a href="location.html" title="View all" class="gradient-button">View all</a>
            <h5>Prague</h5>
            <span class="count">829 Hotels</span>
            <div class="ribbon">
              <div class="half hotel">
                <a href="hotels.html" title="View all">
                  <span class="small">from</span>
                  <span class="price">&#36; 70</span>
                </a>
              </div>
              <div class="half flight">
                <a href="flights.html" title="View all">
                  <span class="small">from</span>
                  <span class="price">&#36; 150</span>
                </a>
              </div>
            </div>
          </div>
        </article>--}}
        <!--//column-->
      </section>

    </div>
  </div>
</div>
@endsection
