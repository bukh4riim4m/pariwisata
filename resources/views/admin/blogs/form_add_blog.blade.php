@extends('layouts.admin_app')

@section('content')
<div class="main" role="main">
  <div class="wrap clearfix">
    <!--main content-->
    <div class="content clearfix">
      <!--breadcrumbs-->
      <nav role="navigation" class="breadcrumbs clearfix">
        <!--crumbs-->
        <ul class="crumbs">
          <li><a href="{{route('home')}}" title="Home">Home</a></li>
          <li><a href="{{route('admin-blog')}}" title="Blog">Blog</a></li>
          <li><a href="#" title="Add Blog">Add Blog</a></li>
        </ul>
        <ul class="top-right-nav">
					<!-- <li><a href="#" title="Back to results"><button  type="button" class="gradient-button" name="button">TAMBAH</button></a></li> -->
					<li><a href="{{route('admin-blog')}}" title="Back">Back</a></li>
				</ul>
        <!--//crumbs-->

      </nav>
      <!--three-fourth content-->
        <section class="three-fourth">
          <form id="booking" method="post" action="{{route('admin-add-blog')}}" class="booking" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="action" value="add">
            <fieldset>
              <h3><span>ADD </span>BLOG</h3>
              <div class="row">
                <div class="f-item">
                  <label for="Title Blog">Title Blog</label>
                  <input type="text" name="title" required/>
                </div>
                <span class="info">Judul Blog</span>
              </div>

              <div class="row">
                <div class="f-item">
                  <label for="">Description</label>
                  <textarea name="deskripsi" id="summernote" rows="8" cols="80" required></textarea>
                  <!-- <textarea id="summernote" name="deskripsi" rows="8" cols="80"></textarea> -->
                </div>
                <span class="info">Deskripsi lengkap tentang paket</span>
              </div>
              <hr />
              <input type="submit" class="gradient-button" value="S I M P A N" id="next-step" />
            </fieldset>
          </form>
        </section>
      <!--//three-fourth content-->

      <!--right sidebar-->
      @include('includes.right_sidebar')
      <!--//right sidebar-->
    </div>
  </div>
</div>
@endsection
