@extends('layouts.admin_app')

@section('content')
<div class="main" role="main">
  <div class="wrap clearfix">
    <!--main content-->
    <div class="content clearfix">
      <!--breadcrumbs-->
      <nav role="navigation" class="breadcrumbs clearfix">
        <!--crumbs-->
        <ul class="crumbs">
          <li><a href="{{route('home')}}" title="Home">Home</a></li>
          <li>Blog</li>
        </ul>
        <ul class="top-right-nav">
					<li><a href="{{route('admin-add-blog')}}" title="Tambah Blog"><button  type="button" class="gradient-button" name="button">+ BLOG BARU</button></a></li>
				</ul>
        <!--//crumbs-->
      </nav>
      <div class="row">
				<!--three-fourth content-->
				<div class="three-fourth">
					<!--post-->
          @foreach($blogs as $key => $blog)
					<article class="static-content post" style="background-color:#ffffff;margin-bottom:15px;padding:10px;">
							<h2><a href="">{{$blog->title}}</a></h2>
							<p class="entry-meta">
								<span class="date">Date: {{date('d M Y H:i', strtotime($blog->updated_at))}}</span>
							</p>
						<div class="entry-content">
							<p>{!!$blog->description!!}</p>
							<a href="{{route('admin-blog-edit',$blog->id)}}" class="gradient-button">Edit</a> <a href="#hapus{{$key}}" title="Delete" class="gradient-button edit">Delete</a>
						</div>
            <div class="edit_field" id="hapus{{$key}}">
              <form class="" action="{{route('admin-blog-edit',$blog->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                <label for="">Yakin dihapus ?</label><br>
                <input type="hidden" name="action" value="hapus" required>
                <input type="submit" value="Ya" class="gradient-button"/>
                <a href="#">Tidak</a>
              </form>
            </div>
					</article>
          @endforeach
					<!--//post-->
          @if(count($blogs) < 1)
          <article class="static-content post" style="background-color:#ffffff;margin-bottom:15px;padding:10px;">
						<div class="entry-content">
							<p style="text-align:center;">Belum ada postingan</p>
						</div>
					</article>
          @endif
          {{$blogs->render()}}

				</div>
        @include('includes.right_sidebar')
			</div>
    </div>
  </div>
</div>
@endsection
